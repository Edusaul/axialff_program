#ifndef _FLUXFIT_H_
#define _FLUXFIT_H_

#include <cmath>
#include <iostream>

namespace flx_fit{
	double fluxfit_ANL(double);
	double fluxfit_BNL(double);
	double fluxfit_FNAL(double);
	double fluxfit_BEBC(double);
}

#endif
