//
// Created by eduardo on 11/06/18.
//

#ifndef AXIALFF_FLUX_CLASS_H
#define AXIALFF_FLUX_CLASS_H

#include <Call_Function.h>

using namespace std;

class flux_class : public Call_Function_Base {
protected:
	string path;
	int nbins;
	vector<double> EnuBinMin;
	vector<double> EnuBinMax;
	vector<double> dNdEnu;
	vector<double> sizeBin;
	double (*fluxfit)(double); //pointer to the fit function
	double dimensions; // 1. -> MeV, 1.0e3 -> GeV
	double total; // total number of events
	bool usefluxfit; // true use fluxflit, false use binned flux data
public:
	//Constructor
	flux_class(string, int, double (*)(double), double, bool);
	//Method
	double Call_Fnc(double) override; // Imput Enu in MeV
};

#endif //AXIALFF_FLUX_CLASS_H
