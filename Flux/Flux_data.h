//
// Created by eduardo on 11/06/18.
//

#ifndef AXIALFF_FLUX_DATA_H
#define AXIALFF_FLUX_DATA_H

#include <string>

namespace Flux_data {
// Flux data
    const std::string ANL_path = "../Aaron_data/ANL_1979_E3C.dat";
    const int ANL_nbins = 500;
    const double ANL_dimensions = 1.0e3;
    const bool ANL_usefluxfit = true;

    const std::string BNL_path = "../Aaron_data/BNL_1981_E3C.dat";
    const int BNL_nbins = 500;
    const double BNL_dimensions = 1.0e3;
    const bool BNL_usefluxfit = true;

    const std::string FNAL_path = "../Aaron_data/FNAL_1983_E3C.dat";
    const int FNAL_nbins = 500;
    const double FNAL_dimensions = 1.0e3;
    const bool FNAL_usefluxfit = true;

// BEBC
    const std::string BEBC_path = "../Aaron_data/BEBC_Barlag_nu_calc_rescan.dat";
// 	const std::string BEBC_path="../Aaron_data/BEBC_Wachsmuth_numu_table.dat";
    const int BEBC_nbins = 22;
    const double BEBC_dimensions = 1.0e3;
    const bool BEBC_usefluxfit = true;


// Events data
    const std::string ANL_data_path = "../Aaron_data/ANLN_Rebinned.dat";
// 	const std::string ANL_data_path="../Aaron_data/ANL_1982_N_2.dat";
    const int ANL_data_nbins = 25;
// 	const double ANL_data_nbins=49;
    const double ANL_data_tot = 1792.0; //total number of events
    const double ANL_data_EnuAv = 953.982; //MeV //averaged energy of the flux
// Asigning values to some parameters
    const double ANL_Enumin = 0.0;
    const double ANL_Enumax = 5000.0;

    const std::string BNL_data_path = "../Aaron_data/BNLN_Rebinned.dat";
// 	const double BNL_data_nbins=22; //Furuno
// 	const dataBNL.nbins=49;
    const int BNL_data_nbins = 28;
// 	const double BNL_data_tot=2780.17; //Furuno
    const double BNL_data_tot = 1236.0; //total number of events
    const double BNL_data_EnuAv = 1567.16; //MeV //averaged energy of the flux
// Asigning values to some parameters
    const double BNL_Enumin = 0.0;
    const double BNL_Enumax = 6000.0;

    const std::string FNAL_data_path = "../Aaron_data/FNALN_Rebinned_2.dat";
    const int FNAL_data_nbins = 17;
    const double FNAL_data_tot = 354.0; //354.0; //total number of events
    const double FNAL_data_EnuAv = 22631.5; //MeV //averaged energy of the flux
// Asigning values to some parameters
    const double FNAL_Enumin = 2300.0;
    const double FNAL_Enumax = 100000.0;

// BEBC
    const std::string BEBC_data_path = "../Aaron_data/dsdQ2_Allasia.dat";
    const int BEBC_data_nbins = 9;
// 	const double BEBC_data_tot=2.7348; //total number of events
// 	const double BEBC_data_EnuAv=8064.7; //MeV //averaged energy of the flux
// Asigning values to some parameters
    const double BEBC_data_factor = 1e-44;
    const double BEBC_Enumin = 5000.0;
    const double BEBC_Enumax = 200000.0;
    const double lep_momentum_cut = 4000.0;
    const int BEBC_bin_div = 20;

}

namespace Param
{
    const int n_of_dist=4;
    extern int BIN; // don't use the first BIN bins in the error calculation
}

#endif //AXIALFF_FLUX_DATA_H
