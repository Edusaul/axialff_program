//
// Created by eduardo on 11/06/18.
//

#include <iostream>
#include <fstream>
#include <Parameters_MeV.h>
#include "Flux_class.h"

//Constructor
flux_class::flux_class(string path, int nbins, double (*fluxfit)(double), double dimensions, bool usefluxfit){
	this->path=path;
	this->nbins=nbins;
	this->fluxfit=fluxfit;
	this->dimensions=dimensions; // 1. -> MeV, 1.0e3 -> GeV
	this->usefluxfit=usefluxfit;

	double dataX,dataY,dataZ;
	ifstream file(path.c_str());//opening imput document
	if(!file) cout<<"WARNING! flux file \""<<path<<"\" not found"<<endl;
	int i;
	double tot=0.0;

	this->EnuBinMin.resize(static_cast<unsigned long>(this->nbins));
	this->EnuBinMax.resize(static_cast<unsigned long>(this->nbins));
	this->dNdEnu.resize(static_cast<unsigned long>(this->nbins));
	this->sizeBin.resize(static_cast<unsigned long>(this->nbins));

	for (i=0;i<this->nbins;i++){
		file>>dataX;
		file>>dataY;
		file>>dataZ;
		this->EnuBinMin[i]=dataX*this->dimensions;
		this->EnuBinMax[i]=dataY*this->dimensions;
		this->dNdEnu[i]=dataZ;
		this->sizeBin[i]=this->EnuBinMax[i]-this->EnuBinMin[i];
		tot+=this->dNdEnu[i];
	}
	this->total=tot;
	file.close();
}

//Method
double flux_class::Call_Fnc(double Enu){
	// Imput Enu in MeV
	if(this->usefluxfit){
		return (fluxfit)(Enu);
	} else {
		double aux=-1.0;
		double res;
		int i;

		for(i=0;i<this->nbins;i++){
			if(Enu<this->EnuBinMax[i]){
				aux=this->dNdEnu[i];
				break;
			}
		}
		if(aux>0.) res=aux;
		else res=0.0;
		return res;
	}
}