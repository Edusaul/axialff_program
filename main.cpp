#include <sstream>
#include "Minuit_class.h"
#include "Fluxfit.h"
#include "Error_class.h"
#include "KNN_output.h"
#include "write_class.h"
#include "Random_class.h"
#include "Plot.h"
#include "Bootstrap.h"
#include "Flux_data.h"
#include "FA_dipole.h"
#include "FA_1_monopole.h"
#include "FA_2_monopoles.h"
#include "FA_tripole.h"
#include "FFV.h"
#include "dsigmadQ2.h"
#include "sigma_tot.h"
#include "dsigdQ2dEnu.h"
#include "Flux_class.h"

using namespace std;
using namespace Flux_data;

calc_error *Array_of_classes[Param::n_of_dist];
Call_Function_Base *FA_pointer;
std::vector<bool> min_experiment(Param::n_of_dist);
int Param::BIN;

template <typename T>
std::string to_string(T value)
{
    std::ostringstream os ;
    os << value ;
    return os.str() ;
}

//Main program
int main()
{
    int i;
    clock_t time1,time2;
    time1= clock(); //Start timer
    bool use_Nfit(Param::n_of_dist);

/////////////////////////////////////////////////////////////////////////
    // OPTIONS for the program

    // Activate (true) / deactivate (false) write event distributions
    bool do_writeDistributions=false;
    bool error_bands=true;

    // Activate (true) / deactivate (false) calculate chi2
    bool calculate_chi2=false;
    // Activate (true) / deactivate (false) use NN output
    bool use_NN_out=false;

    // Activate (true) / deactivate (false) minuit
    bool do_minuit=true;
    // Activate (true) / deactivate (false) bootstrap with minuit options
// 		bool do_Bootstrap=true;
    int N_minuits = 0; // 0 -> just 1 minimization ; >0 -> do Bootstrap
    bool add_error_correction=true;
    bool do_Minos=true;
    int BIN=0; // don't use the first BIN bins in the error calculation
    Param::BIN=BIN;

    bool one_monopole=false;
    bool two_monopoles=false;
    bool tripole=false;

    bool logLL_error_function=false;
    // Activate (true) / deactivate (false) each experiment for minuit
    min_experiment[0]=true; //ANL
    min_experiment[1]=false; //BNL
    min_experiment[2]=false; //FNAL
    min_experiment[3]=false; //BEBC

    use_Nfit=true; //if false N_fit will be constant in minuit minimization

    //Choice for Singh correction: 0 -> off, 1 -> parameter outside integrals,
    // 2 -> Inside all cross section calculations (including integrated one)
    int choiceSinghANL=2;
    int choiceSinghBNL=1;
    int choiceSinghFNAL=2;
    int choiceSinghBEBC=2;

/////////////////////////////////////////////////////////////////////////
    // path to write the distributions

    std::string FF_file_ANL_path="../Data/dsdpK_ANL_diple.dat"; //path for opening output document
    std::string FF_file_BNL_path="../Data/dsdpK_BNL.dat"; //path for opening output document
    std::string FF_file_FNAL_path="../Data/dsdpK_FNAL.dat"; //path for opening output document
    std::string FF_file_BEBC_path="../Data/dsdpK_BEBC.dat"; //path for opening output document
    std::string FF_file_FA_path="../Data/FA.dat"; //path for opening output document

    std::string FF_file_FA_Bootstrap_path="../Data/FA_Bootstrap_b.dat"; //path for opening output document

/////////////////////////////////////////////////////////////////////////

    // Axial mass and incident neutrino energy to write distributions
    std::vector<double> MA(2);
    std::vector<double> Delta_MA_plus(2);
    std::vector<double> Delta_MA_minus(2);
// 	MA[0]=600.0; // 1pole
    MA[0]=1045.5; // 2pole
// 	MA[0]=1435.26; // 3pole
    MA[1]=100.0;
    Delta_MA_minus[0]=60.0;
    Delta_MA_minus[1]=0.0;
    Delta_MA_plus[0]=60.0;
    Delta_MA_plus[1]=0.0;

    double Enu=1000.0;
    double ANL_Nfit=1.0938;//Nfit;
    double BNL_Nfit=1.12;//Nfit;
    double FNAL_Nfit=1.09;//Nfit;
    double BEBC_Nfit=0.65;//Nfit;
    double ANL_dn=0.2; // normalization constant error
    double BNL_dn=0.2; // normalization constant error
    double FNAL_dn=0.2; // normalization constant error


/////////////////////////////////////////////////////////////////////////
    // Declaration of classes
    FFV FF_V(1);
// 	KNN_FV FF_V(1);
    FA_dipole FA_dip;
    FA_1_monopole FA_1_mono;
    FA_2_monopoles FA_monopoles;
    FA_tripole FA_3pole;

    FA_pointer=&FA_dip;
    if(one_monopole==true) FA_pointer=&FA_1_mono;
    if(two_monopoles==true) FA_pointer=&FA_monopoles;
    if(tripole==true) FA_pointer=&FA_3pole;
    
    KNN_FA_NEW KNN;
    if(use_NN_out==true) {
        FA_pointer=&KNN;
    }
    dsigmadQ2 dsdQ2(&FF_V,FA_pointer,false);
    Integration_Class dsdQ2_integral(&dsdQ2);
    sigma_tot sigma(&dsdQ2_integral);


    flux_class ANL_flux(ANL_path,ANL_nbins,&flx_fit::fluxfit_ANL,ANL_dimensions,ANL_usefluxfit);
    flux_class BNL_flux(BNL_path,BNL_nbins,&flx_fit::fluxfit_BNL,BNL_dimensions,BNL_usefluxfit);
    flux_class FNAL_flux(FNAL_path,FNAL_nbins,&flx_fit::fluxfit_FNAL,FNAL_dimensions,FNAL_usefluxfit);

    flux_class BEBC_flux(BEBC_path,BEBC_nbins,&flx_fit::fluxfit_BEBC,BEBC_dimensions,BEBC_usefluxfit);

    dNdQ2dEnu dN_ANL(&dsdQ2,&ANL_flux,&sigma,choiceSinghANL);
    dNdQ2dEnu dN_BNL(&dsdQ2,&BNL_flux,&sigma,choiceSinghBNL);
    dNdQ2dEnu dN_FNAL(&dsdQ2,&FNAL_flux,&sigma,choiceSinghFNAL);

    dsigdQ2dEnu ds_BEBC(&dsdQ2,&BEBC_flux,&sigma,choiceSinghBEBC);

    Integration_Class Nth_ANL(&dN_ANL);
    Integration_Class Nth_BNL(&dN_BNL);
    Integration_Class Nth_FNAL(&dN_FNAL);
    Integration_Class int_flux_ANL(&ANL_flux);
    Integration_Class int_flux_BNL(&BNL_flux);
    Integration_Class int_flux_FNAL(&FNAL_flux);

    Integration_Class Sig_th_BEBC(&ds_BEBC);
    Integration_Class int_flux_BEBC(&BEBC_flux);

/////////////////////////////////////////////////////////////////////////
    // Integration of flux for normalization
    double ANL_total_flux=int_flux_ANL.Integrate(ANL_Enumin,ANL_Enumax,1e-1);
    double BNL_total_flux=int_flux_BNL.Integrate(BNL_Enumin,BNL_Enumax,1e-1);
    double FNAL_total_flux=int_flux_FNAL.Integrate(FNAL_Enumin,FNAL_Enumax,1e-1);

    double BEBC_total_flux=int_flux_BEBC.Integrate(BEBC_Enumin,BEBC_Enumax,1e-1);
// 	cout<<"Flux integral = "<<BEBC_total_flux<<endl;

/////////////////////////////////////////////////////////////////////////
    // Declaration of the class which calculate the event distribution
    dNdQ2_data ANL_distribution(ANL_data_path,ANL_data_nbins,ANL_Nfit,ANL_data_tot,ANL_total_flux,ANL_data_EnuAv,ANL_Enumin,ANL_Enumax,&Nth_ANL);
    dNdQ2_data BNL_distribution(BNL_data_path,BNL_data_nbins,BNL_Nfit,BNL_data_tot,BNL_total_flux,BNL_data_EnuAv,BNL_Enumin,BNL_Enumax,&Nth_BNL);
    dNdQ2_data FNAL_distribution(FNAL_data_path,FNAL_data_nbins,FNAL_Nfit,FNAL_data_tot,FNAL_total_flux,FNAL_data_EnuAv,FNAL_Enumin,FNAL_Enumax,&Nth_FNAL);

    dsdQ2_data BEBC_diff_cs_avg(BEBC_data_path,BEBC_data_nbins,BEBC_Nfit,BEBC_bin_div,BEBC_total_flux,BEBC_Enumin,BEBC_Enumax,lep_momentum_cut,BEBC_data_factor,&Sig_th_BEBC);


/////////////////////////////////////////////////////////////////////////

    // Writing event distribution
    if(do_writeDistributions==true) {

        write_distribution write_ANL(&ANL_distribution, FF_file_ANL_path, FA_pointer, MA, Delta_MA_plus, Delta_MA_minus, error_bands);
        write_distribution write_BNL(&BNL_distribution, FF_file_BNL_path, FA_pointer, MA, Delta_MA_plus, Delta_MA_minus, error_bands);
        write_distribution write_FNAL(&FNAL_distribution, FF_file_FNAL_path, FA_pointer, MA, Delta_MA_plus, Delta_MA_minus, error_bands);
        write_distribution_dsdQ2 write_BEBC(&BEBC_diff_cs_avg, FF_file_BEBC_path, FA_pointer, MA, Delta_MA_plus, Delta_MA_minus, error_bands);

        write_ANL.write();
        write_BNL.write();
        write_FNAL.write();
        write_BEBC.write();

        write_FA write_FA(FF_file_FA_path, FA_pointer, MA, Delta_MA_plus, Delta_MA_minus, error_bands);
        write_FA.write(0.0,3e6,50);
    }

/////////////////////////////////////////////////////////////////////////

    //Calculation of chi2
    if(calculate_chi2==true) {
        error_chi2 er_ANL_chi2(&ANL_distribution,ANL_dn,add_error_correction);
        error_chi2 er_BNL_chi2(&BNL_distribution,BNL_dn,add_error_correction);
        error_chi2 er_FNAL_chi2(&FNAL_distribution,FNAL_dn,add_error_correction);
        cout<<"ChiSq for ANL =  "<<er_ANL_chi2.error(BIN,1e-3,20)<<endl;
        cout<<"ChiSq for BNL =  "<<er_BNL_chi2.error(BIN,1e-3,20)<<endl;
        cout<<"ChiSq for FNAL =  "<<er_FNAL_chi2.error(BIN,1e-3,20)<<endl;

        error_chi2_dsdQ2 er_BEBC_chi2(&BEBC_diff_cs_avg);
        cout<<"ChiSq for BEBC =  "<<er_BEBC_chi2.error(BIN,1e-50,50)<<endl;
    }

/////////////////////////////////////////////////////////////////////////

    //Minuit
    if(do_minuit==true) {
        // Calculating error for the distribution

        minuit_minimization *minuit_pointer;

        int number_of_parameters=1;
        for(i=0;i<4;i++) {
            if(min_experiment[i]==true) {
                number_of_parameters+=1;
            }
        }
        if(two_monopoles==true) number_of_parameters+=1;

        synthetic_dNdQ2_data synct_ANL(&ANL_distribution);
        synthetic_dNdQ2_data synct_BNL(&BNL_distribution);
        synthetic_dNdQ2_data synct_FNAL(&FNAL_distribution);

        synthetic_dsdQ2_data synct_BEBC(&BEBC_diff_cs_avg);

        error_chi2 er_ANL_chi2(&synct_ANL,ANL_dn,add_error_correction);
        error_chi2 er_BNL_chi2(&synct_BNL,BNL_dn,add_error_correction);
        error_chi2 er_FNAL_chi2(&synct_FNAL,FNAL_dn,add_error_correction);

        error_logLL er_ANL_logLL(&synct_ANL);
        error_logLL er_BNL_logLL(&synct_BNL);
        error_logLL er_FNAL_logLL(&synct_FNAL);

        calc_error *er_ANL=&er_ANL_chi2;
        calc_error *er_BNL=&er_BNL_chi2;
        calc_error *er_FNAL=&er_FNAL_chi2;

        error_chi2_dsdQ2 er_BEBC_chi2(&synct_BEBC);
        calc_error *er_BEBC=&er_BEBC_chi2;

        if(logLL_error_function==true){
            er_ANL=&er_ANL_logLL;
            er_BNL=&er_BNL_logLL;
            er_FNAL=&er_FNAL_logLL;
        }

        Array_of_classes[0]=er_ANL;
        Array_of_classes[1]=er_BNL;
        Array_of_classes[2]=er_FNAL;
        Array_of_classes[3]=er_BEBC;

        // dipole and 1 monopole hasve the same number of parameters, so the same class can be used for both
        minuit_minimization_dipole minuit_dipole(number_of_parameters,min_experiment,use_Nfit,do_Minos);
        minuit_minimization_2_monopoles minuit_2_monopoles(number_of_parameters,min_experiment);
        if(two_monopoles==false) minuit_pointer=&minuit_dipole;
        else minuit_pointer=&minuit_2_monopoles;

        minuit_pointer->minimize();

        std::vector<double> param_vect_1 (N_minuits+1);
        std::vector<double> param_vect_2 (N_minuits+1);
        std::vector<double> param_vect_3 (N_minuits+1);
        std::vector<double> param_vect_4 (N_minuits+1);
        std::vector<double> param_vect_5 (N_minuits+1);
        std::vector<double> param_vect_6 (N_minuits+1);

        for(int j=0;j<N_minuits+1;j++){

            minuit_pointer->minimize();
            param_vect_1[j]=minuit_pointer->out_par[0];
            if(number_of_parameters>1) {
                param_vect_2[j]=minuit_pointer->out_par[1];
                if(number_of_parameters>2) {
                    param_vect_3[j]=minuit_pointer->out_par[2];
                    if(number_of_parameters>3) {
                        param_vect_4[j]=minuit_pointer->out_par[3];
                        if(number_of_parameters>4) {
                            param_vect_5[j]=minuit_pointer->out_par[4];
                            if(number_of_parameters>5) {
                                param_vect_6[j]=minuit_pointer->out_par[5];
                            }
                        }
                    }
                }
            }
            synct_ANL.Generate_synct_data();
            synct_BNL.Generate_synct_data();
            synct_FNAL.Generate_synct_data();
            synct_BEBC.Generate_synct_data();
        }

        if(N_minuits>0){

            std::vector<double> param_orig_data = {param_vect_1[0],param_vect_2[0],param_vect_3[0],param_vect_4[0],param_vect_5[0],param_vect_6[0]};

            write_FA_Bootstrap FA_Bootstrap_write(FF_file_FA_Bootstrap_path, FA_pointer, N_minuits, param_vect_1, param_vect_2);

            std::sort(param_vect_1.begin(), param_vect_1.end());
            std::sort(param_vect_2.begin(), param_vect_2.end());
            std::sort(param_vect_3.begin(), param_vect_3.end());
            std::sort(param_vect_4.begin(), param_vect_4.end());
            std::sort(param_vect_5.begin(), param_vect_5.end());
            std::sort(param_vect_6.begin(), param_vect_6.end());

            int nlow  = int(0.16*N_minuits);
            int nhigh = N_minuits - int(0.16*N_minuits);

            std::vector<double> param_vect_nlow = {param_vect_1[nlow],param_vect_2[nlow],param_vect_3[nlow],param_vect_4[nlow],param_vect_5[nlow],param_vect_6[nlow]};
            std::vector<double> param_vect_nhigh = {param_vect_1[nhigh],param_vect_2[nhigh],param_vect_3[nhigh],param_vect_4[nhigh],param_vect_5[nhigh],param_vect_6[nhigh]};

            for(int j=0;j<number_of_parameters;j++){
                cout<<endl<<minuit_pointer->parName[j]<<" = "<<param_orig_data[j]<<" - "<<param_orig_data[j]-param_vect_nlow[j]<<" + "<<-param_orig_data[j]+param_vect_nhigh[j]<<" ; "<<param_vect_nlow[j]<<" < "<<param_orig_data[j]<<" < "<<param_vect_nhigh[j]<<endl;
            }

            FA_Bootstrap_write.write(0.0,3e6);
        }

    }

/////////////////////////////////////////////////////////////////////////


// 	KNN_FV ffv(1);
//
// 	FFV ffv_dip(1);
//
// 	std::string FF_file_FV_path="Data/FFV.dat"; //path for opening output document
// 	ofstream FF_file(FF_file_FV_path.c_str()); //opening output document
//
// 	std::string FF_file_FVD_path="Data/FFV_dip.dat"; //path for opening output document
// 	ofstream FFD_file(FF_file_FVD_path.c_str()); //opening output document
//
// 	double Q2min=0.0;
// 	double Q2max=3.0e6;
// 	int Npts=50;
//
// 	double Q2h=(Q2max-Q2min)/double(Npts-1);
// 	double Q2=Q2min;
// 	for(int i=0;i<Npts;i++){
// 		ffv.Call_Fnc(Q2);
// 		FF_file<<Q2*1.0e-6<<"   "<<ffv.G[0]<<"   "<<ffv.G[1]<<"   "<<ffv.G[2]<<"   "<<ffv.G[3]<<"   "<<ffv.F1v<<"   "<<ffv.F2v<<endl;
//
// 		ffv_dip.Call_Fnc(Q2);
// 		FFD_file<<Q2*1.0e-6<<"   "<<ffv_dip.G[0]<<"   "<<ffv_dip.G[1]<<"   "<<ffv_dip.G[2]<<"   "<<ffv_dip.G[3]<<"   "<<ffv_dip.F1v<<"   "<<ffv_dip.F2v<<endl;
//
// // 		cout<<Q2*1.0e-6<<"   "<<ffv_dip.F1v<<"   "<<ffv.F1v<<"   "<<ffv_dip.F2v<<"   "<<ffv.F2v<<endl;
//
// 		Q2 += Q2h;
// 	}


/////////////////////////////////////////////////////////////////////////

    time2= clock(); //timer
    float diff;
    diff= ((float)time2-(float)time1)/CLOCKS_PER_SEC;
    cout<<endl<<"Time"<<" "<<diff<<" "<<"s"<<endl;

    return 0;
}
