#ifndef _MINUIT_H_
#define _MINUIT_H_

#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <vector>
#include <iomanip>

#include <TMinuit.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TF1.h>
#include <TAxis.h>
#include <TLine.h>

#include "Integration.h"
#include "Call_Function.h"
#include <Parameters_MeV.h>
#include "Error_class.h"
#include "Flux_data.h"

// extern const int n_of_dist;
extern calc_error *Array_of_classes[Param::n_of_dist];
extern Call_Function_Base *FA_pointer;
extern std::vector<bool> min_experiment;

class minuit_minimization {
protected:
	std::vector<double> par;
	std::vector<double> stepSize;
	std::vector<double> minVal;
	std::vector<double> maxVal;
	std::vector<bool> min_experiment;
	bool use_Nfit;
	bool do_Minos;
	
public:
	int number_of_parameters;
	std::vector<string> parName;
	std::vector<double> out_par;
	std::vector<double> err_par;
	//Constructor
	minuit_minimization();
	//Destructor
	virtual ~minuit_minimization();
	//Method
	virtual double minimize();
};

///////////////////////////////////////////////////////////////////
// minimization dipole

class minuit_minimization_dipole : public minuit_minimization {
protected:

	
public:
	//Constructor
	minuit_minimization_dipole(int,std::vector<bool>,bool,bool);
	//Destructor
	~minuit_minimization_dipole() override;
	//Method
	double minimize() override;
};

void fcn_dipole(int &npar, double *deriv, double &f, double par[], int flag);

///////////////////////////////////////////////////////////////////
// minimization 2 monopoles

class minuit_minimization_2_monopoles : public minuit_minimization {
protected:

	
public:
	//Constructor
	minuit_minimization_2_monopoles(int,std::vector<bool>);
	//Destructor
	~minuit_minimization_2_monopoles() override;
	//Method
	double minimize() override;
};

void fcn_2_monopoles(int &npar, double *deriv, double &f, double par[], int flag);

#endif
