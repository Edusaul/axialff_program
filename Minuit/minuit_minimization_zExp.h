//
// Created by edusaul on 18/06/18.
//

#ifndef AXIALFF_MINUIT_MINIMIZATION_ZEXP_H
#define AXIALFF_MINUIT_MINIMIZATION_ZEXP_H

#include "Minuit_class.h"

class minuit_minimization_zExp : public minuit_minimization {
public:
    minuit_minimization_zExp(int,std::vector<bool>,bool,bool);

    double minimize() override;
};

void fcn_zExp(int &npar, double *deriv, double &f, double par[], int flag);

namespace Param{
    extern int number_of_a_zExp;
}

#endif //AXIALFF_MINUIT_MINIMIZATION_ZEXP_H
