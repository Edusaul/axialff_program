#ifndef _ERROR_CLASS_H_
#define _ERROR_CLASS_H_

#include "dNdQ2_data.h"
#include "dsdQ2_data.h"

using namespace std;

class calc_error {
protected:
public:
	//Constructor
	calc_error();	
	//Destructor
	virtual ~calc_error();
	//Method
	virtual double error(int,double, int);
	virtual void change_parameters(double);
};


class error_chi2 : public calc_error {
protected:
	dNdQ2_data *dist_Events;
	bool add_error_correction;
	double dn;
public:
	//Constructor
	explicit error_chi2(dNdQ2_data*, double dn = 0.2, bool add_error_correction = false);
	//Destructor
	~error_chi2() override;
	//Method
	double error(int BIN=0, double precission=1e-3, int iter=10) override;
	void change_parameters(double) override;
};	

class error_logLL : public calc_error {
protected:
	dNdQ2_data *dist_Events;
public:
	//Constructor
	explicit error_logLL(dNdQ2_data*);
	//Destructor
	~error_logLL() override;
	//Method
	double error(int BIN=0, double precission=1e-3, int iter=10) override;
	void change_parameters(double) override;
};


class error_chi2_dsdQ2 : public calc_error {
protected:
	dsdQ2_data *diff_cs;
// 	bool add_error_correction;
// 	double dn;
public:
	//Constructor
	explicit error_chi2_dsdQ2(dsdQ2_data*);
	//Destructor
	~error_chi2_dsdQ2() override;
	//Method
	double error(int BIN=0, double precission=1e-3, int iter=10) override;
	void change_parameters(double) override;
};	

#endif