// Class minuit_minimization
#include "Minuit_class.h"


minuit_minimization::minuit_minimization(){}
minuit_minimization::~minuit_minimization(){}
double minuit_minimization::minimize(){}

///////////////////////////////////////////////////////////////////
// minimization dipole

///////////////////////////////////////////////////////////////////
// class minuit_minimization_dipole

//Constructor
minuit_minimization_dipole::minuit_minimization_dipole(int number_of_parameters,std::vector<bool> min_experiment,bool use_Nfit, bool do_Minos){
	this->number_of_parameters=number_of_parameters;
	this->min_experiment=min_experiment;
	this->use_Nfit=use_Nfit;
	this->do_Minos=do_Minos;
	
	this->par.resize(static_cast<unsigned long>(number_of_parameters));
	this->stepSize.resize(static_cast<unsigned long>(number_of_parameters));
	this->minVal.resize(static_cast<unsigned long>(number_of_parameters));
	this->maxVal.resize(static_cast<unsigned long>(number_of_parameters));
	this->parName.resize(static_cast<unsigned long>(number_of_parameters));
	this->out_par.resize(static_cast<unsigned long>(number_of_parameters));
	this->err_par.resize(static_cast<unsigned long>(number_of_parameters));
	
	this->par[0] = 1000.0;            // a guess
	this->stepSize[0] = 10.0;       // take e.g. 0.1 of start value
	this->minVal[0] = 0.1;   // if min and max values = 0, parameter is unbounded.
	this->maxVal[0] = 10000.0;
	this->parName[0] = "MA";
	
	if(number_of_parameters>1) {
		this->par[1] = 1.0;            // a guess
		this->stepSize[1] = 0.1;       // take e.g. 0.1 of start value
		this->minVal[1] = 0.01;   // if min and max values = 0, parameter is unbounded.
		this->maxVal[1] = 100.0;
		this->parName[1] = "Nfit_1";
		
		if(number_of_parameters>2) {
			this->par[2] = 1.0;            // a guess
			this->stepSize[2] = 0.1;       // take e.g. 0.1 of start value
			this->minVal[2] = 0.01;   // if min and max values = 0, parameter is unbounded.
			this->maxVal[2] = 100.0;
			this->parName[2] = "Nfit_2";
			
			if(number_of_parameters>3) {
				this->par[3] = 1.0;            // a guess
				this->stepSize[3] = 0.1;       // take e.g. 0.1 of start value
				this->minVal[3] = 0.01;   // if min and max values = 0, parameter is unbounded.
				this->maxVal[3] = 100.0;
				this->parName[3] = "Nfit_3";
				
				if(number_of_parameters>4) {
					this->par[4] = 1.0;            // a guess
					this->stepSize[4] = 0.1;       // take e.g. 0.1 of start value
					this->minVal[4] = 0.01;   // if min and max values = 0, parameter is unbounded.
					this->maxVal[4] = 100.0;
					this->parName[4] = "Nfit_4";
				}
			}
		}
	}
	
	if(!use_Nfit){
		for (int i=1; i<this->number_of_parameters; i++){
			this->stepSize[i]=0;
		}
	}
	
}
//Destructor
minuit_minimization_dipole::~minuit_minimization_dipole() = default;

//Method
double minuit_minimization_dipole::minimize(){
	
	TMinuit minuit(number_of_parameters);
	minuit.SetFCN(fcn_dipole);
	
	for (int i=0; i<this->number_of_parameters; i++){
    minuit.DefineParameter(i, this->parName[i].c_str(), 
      this->par[i], this->stepSize[i], this->minVal[i], this->maxVal[i]);
	}
	
// 	minuit.Migrad();       // Minuit's best minimization algorithm
	double outpar[this->number_of_parameters], err[this->number_of_parameters];
// 	for (int i=0; i<this->number_of_parameters; i++){
// 		minuit.GetParameter(i,outpar[i],err[i]);
// 	}
	double arglist[10];
	int ierflg=0;
// 	minuit.SetPrintLevel(1);
// 	minuit.mnexcm("SET NOW",arglist,1,ierflg);
// 	arglist[0]=1;
	arglist[0]=0;
	minuit.mnexcm("MIGRAD",arglist,0,ierflg);
	if(do_Minos) minuit.mnexcm("MINOS", arglist, 0, ierflg);
	
	for (int i=0; i<this->number_of_parameters; i++){
		minuit.GetParameter(i,outpar[i],err[i]);
	}
	
	double ra=12.0/(outpar[0]/Param::hc)/(outpar[0]/Param::hc);
	double ra_error=err[0]/Param::hc*24.0/(outpar[0]/Param::hc)/(outpar[0]/Param::hc)/(outpar[0]/Param::hc);
	
	for(int i=0; i<this->number_of_parameters; i++){
		this->out_par[i]=outpar[i];
		this->err_par[i]=err[i];
	}
	
	cout<<endl<<"Result of minimization: "<<endl;
	cout<<"	MA = "<<outpar[0]<<" ± "<<err[0]<<"   ->      ra ="<<ra<<" ± "<< ra_error<<endl;
	
// 	cout<<"  & "<<"   & "<<outpar[1]<<" $\\pm$ "<<err[1]<<"  & "<<outpar[0]<<" $\\pm$ "<<err[0]<<"  & "<<ra<<" $\\pm$ "<< ra_error<<"  &   \\\\ \\hline"<<endl;
	
// 	for (int i=1; i<this->number_of_parameters; i++){
// 		if(min_experiment[i-1]==true) cout<<"	"<<this->parName[i]<<" =	"<<outpar[i]<<"	error = "<<err[i]<<endl;
// 	}
	int j=1;
	for (int i=0;i<Param::n_of_dist;i++) {
		if(min_experiment[i]==true){
			cout<<"	"<<this->parName[j]<<" = "<<outpar[j]<<" ± "<<err[j]<<endl;
			j++;
		}
	}
// 	Double_t eplus;
// 	Double_t eminus;
// 	Double_t eparab;
// 	Double_t gcc;
// 	Int_t inttt=0;
// 	minuit.mnerrs(inttt,eplus,eminus,eparab,gcc);
// 	cout<<eplus<<"  "<<eminus<<"  "<<eparab<<"  "<<gcc<<endl;
	
	return 0;
};



///////////////////////////////////////////////////////////////////
// function to do the minimization for a dipole

void fcn_dipole(int &npar, double *deriv, double &f, double par[], int flag) {
// 	std::vector<double> error(n_of_dist);
	int j=1;
	double error=0.0;
// 	bool used[3];
	
	std::vector<double> Param_axial_mass(1);
	Param_axial_mass[0]=par[0];
	FA_pointer->Change_other_imputs(Param_axial_mass);
// 	cout<<"	-> MA = "<<Param_axial_mass[0]<<",	";
	cout<< std::right <<std::setw(12) <<"-> MA = "<<std::setw(8) <<Param_axial_mass[0]<< std::left<<std::setw(4) <<" ,";
	
	double Param_normalization;
	for (int i=0;i<Param::n_of_dist;i++){
		if(min_experiment[i]==true){
			Param_normalization=par[j];
			Array_of_classes[i]->change_parameters(Param_normalization);
			error+=Array_of_classes[i]->error(Param::BIN,1e-3,10);
			cout<< std::right <<std::setw(5) <<"Nfit_"<<j<<" = "<< std::setw(8) <<Param_normalization<< std::left<<std::setw(4) <<" ,";
			j++;
		}
	}
	
	f=error;
	cout<< std::right<<"error function = "<<std::setw(7)<<error<<endl;
}

///////////////////////////////////////////////////////////////////
// minimization 2 monopoles

///////////////////////////////////////////////////////////////////
// class minuit_minimization_2_monopoles

//Constructor
minuit_minimization_2_monopoles::minuit_minimization_2_monopoles(int number_of_parameters,std::vector<bool> min_experiment){
	this->number_of_parameters=number_of_parameters;
	this->min_experiment=min_experiment;
	this->use_Nfit=use_Nfit;
	this->do_Minos=do_Minos;
	
	this->par.resize(static_cast<unsigned long>(number_of_parameters));
	this->stepSize.resize(static_cast<unsigned long>(number_of_parameters));
	this->minVal.resize(static_cast<unsigned long>(number_of_parameters));
	this->maxVal.resize(static_cast<unsigned long>(number_of_parameters));
	this->parName.resize(static_cast<unsigned long>(number_of_parameters));
	this->out_par.resize(static_cast<unsigned long>(number_of_parameters));
	this->err_par.resize(static_cast<unsigned long>(number_of_parameters));
	
	this->par[0] = 1000.0;            // a guess
	this->stepSize[0] = 10.0;       // take e.g. 0.1 of start value
	this->minVal[0] = 0.1;   // if min and max values = 0, parameter is unbounded.
	this->maxVal[0] = 10000.0;
	this->parName[0] = "MA";
	
	this->par[1] = 10.0;            // a guess
	this->stepSize[1] = 1.0;       // take e.g. 0.1 of start value
	this->minVal[1] = 0.1;   // if min and max values = 0, parameter is unbounded.
	this->maxVal[1] = 10000.0;
	this->parName[1] = "delta_MA";
	
	if(number_of_parameters>2) {
		this->par[2] = 1.0;            // a guess
		this->stepSize[2] = 0.1;       // take e.g. 0.1 of start value
		this->minVal[2] = 0.01;   // if min and max values = 0, parameter is unbounded.
		this->maxVal[2] = 100.0;
		this->parName[2] = "Nfit_1";
		
		if(number_of_parameters>3) {
			this->par[3] = 1.0;            // a guess
			this->stepSize[3] = 0.1;       // take e.g. 0.1 of start value
			this->minVal[3] = 0.01;   // if min and max values = 0, parameter is unbounded.
			this->maxVal[3] = 100.0;
			this->parName[3] = "Nfit_2";
			
			if(number_of_parameters>4) {
				this->par[4] = 1.0;            // a guess
				this->stepSize[4] = 0.1;       // take e.g. 0.1 of start value
				this->minVal[4] = 0.01;   // if min and max values = 0, parameter is unbounded.
				this->maxVal[4] = 100.0;
				this->parName[4] = "Nfit_3";
				
				if(number_of_parameters>5) {
					this->par[5] = 1.0;            // a guess
					this->stepSize[5] = 0.1;       // take e.g. 0.1 of start value
					this->minVal[5] = 0.01;   // if min and max values = 0, parameter is unbounded.
					this->maxVal[5] = 100.0;
					this->parName[5] = "Nfit_5";
				}
			}
		}
	}
	
	
}
//Destructor
minuit_minimization_2_monopoles::~minuit_minimization_2_monopoles() = default;

//Method
double minuit_minimization_2_monopoles::minimize(){
	
	TMinuit minuit(number_of_parameters);
	minuit.SetFCN(fcn_2_monopoles);
	
	for (int i=0; i<this->number_of_parameters; i++){
    minuit.DefineParameter(i, this->parName[i].c_str(), 
      this->par[i], this->stepSize[i], this->minVal[i], this->maxVal[i]);
	}
	
// 	minuit.Migrad();       // Minuit's best minimization algorithm
	double outpar[this->number_of_parameters], err[this->number_of_parameters];

	double arglist[10];
	int ierflg=0;
	arglist[0]=0;
	minuit.mnexcm("MIGRAD",arglist,0,ierflg);
	if(do_Minos) minuit.mnexcm("MINOS", arglist, 0, ierflg);
	for (int i=0; i<this->number_of_parameters; i++){
		minuit.GetParameter(i,outpar[i],err[i]);
	}
	
	double ra=12.0/(outpar[0]/Param::hc)/(outpar[0]/Param::hc);
	double ra_error=err[0]/Param::hc*24.0/(outpar[0]/Param::hc)/(outpar[0]/Param::hc)/(outpar[0]/Param::hc);
	
	for(int i=0; i<this->number_of_parameters; i++){
		this->out_par[i]=outpar[i];
		this->err_par[i]=err[i];
	}
	
	
	cout<<endl<<"Result of minimization: "<<endl;
	cout<<"	MA = "<<outpar[0]<<" ± "<<err[0]<<"   ->      ra ="<<ra<<"  error = "<< ra_error<<endl;
	cout<<"	delta_MA = "<<outpar[1]<<" ± "<<err[1]<<endl;
	int j=2;
	for (int i=0;i<Param::n_of_dist;i++) {
		if(min_experiment[i]==true){
			cout<<"	"<<this->parName[j]<<" = "<<outpar[j]<<" ± "<<err[j]<<endl;
			j++;
		}
	}
// 	for (int i=2; i<this->number_of_parameters; i++){
// 		if(min_experiment[i-2]==true) cout<<"	"<<this->parName[i]<<" =	"<<outpar[i]<<"	error = "<<err[i]<<endl;
// 	}
	
	return 0;
};



///////////////////////////////////////////////////////////////////
// function to do the minimization for 2 monopoles

void fcn_2_monopoles(int &npar, double *deriv, double &f, double par[], int flag) {
// 	std::vector<double> error(n_of_dist);
	double error=0.0;
	
	std::vector<double> Param_axial_mass(2);
	Param_axial_mass[0]=par[0];
	Param_axial_mass[1]=par[1];
	FA_pointer->Change_other_imputs(Param_axial_mass);
// 	cout<<"	-> MA = "<<Param_axial_mass[0]<<",	";
	cout<< std::right <<std::setw(12) <<"-> MA = "<<std::setw(8) <<Param_axial_mass[0]<< std::left<<std::setw(4) <<" ,";
	
// 	cout<<"	delta_MA = "<<Param_axial_mass[1]<<",	";
	cout<< std::right <<std::setw(12) <<"delta_MA = "<<std::setw(8) <<Param_axial_mass[1]<< std::left<<std::setw(4) <<" ,";
	
	double Param_normalization;
	int j=2;
	for (int i=0;i<Param::n_of_dist;i++){
		if(min_experiment[i]==true){
			Param_normalization=par[j];
			Array_of_classes[i]->change_parameters(Param_normalization);
			error+=Array_of_classes[i]->error(Param::BIN,1e-3,10);
// 			cout<<"	Nfit_"<<j-1<<" = "<<Param_normalization;
			cout<< std::right <<std::setw(5) <<"Nfit_"<<j-1<<" = "<< std::setw(8) <<Param_normalization<< std::left<<std::setw(4) <<" ,";
			j++;
		}
	}
	
// 	for (int i=2; i<npar; i++) {
// 		if(min_experiment[i-2]==true){
// 			Param_normalization=par[i];
// 			Array_of_classes[i-2]->change_parameters(Param_normalization);
// 			error+=Array_of_classes[i-2]->error(1e-3,10);
// 			cout<<"	Nfit_"<<i-1<<" = "<<Param_normalization;
// 		}
// 	}
	
	f=error;
// 	cout<<"	error = "<<error<<endl;
	cout<< std::right<<"error function = "<<std::setw(7)<<error<<endl;
}
