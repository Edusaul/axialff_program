
#include <cmath>
#include <iostream>
#include "Error_class.h"

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//class calc_error

//Constructor
calc_error::calc_error() = default;

//Destructor
calc_error::~calc_error() = default;

//Method
double calc_error::error(int BIN, double precission, int iter){
	return 0;
}
void calc_error::change_parameters(double new_parameters){}


/////////////////////////////////////////////////////////////////////
//class error_chi2

//Constructor
error_chi2::error_chi2(dNdQ2_data *dist_Events, double dn, bool add_error_correction){
	this->dist_Events=dist_Events;
	this->add_error_correction=add_error_correction;
	this->dn=dn;
}	
//Destructor
error_chi2::~error_chi2() = default;

//Method
double error_chi2::error(int BIN, double precission, int iter){
	this->dist_Events->distEvents(precission, iter);
	int i;
	double error=0.0;
	double delta;
	for(i=BIN;i<this->dist_Events->nbins;i++) {
		delta=(this->dist_Events->th_dNdQ2[i]-this->dist_Events->dNdQ2[i])/this->dist_Events->var[i];
		error+=delta*delta;
	}
//	if (this->add_error_correction) error+= (1.0 - this->dist_Events->Nfit) * (1.0 - this->dist_Events->Nfit) / (this->dn * this->dn) + 0.01851236399832205 / (2.0 * 174.0);
	if (this->add_error_correction) error+= (1.0 - this->dist_Events->Nfit) * (1.0 - this->dist_Events->Nfit) / (this->dn * this->dn);
	return error;
}
void error_chi2::change_parameters(double new_parameters){
	this->dist_Events->Nfit=new_parameters;
}


/////////////////////////////////////////////////////////////////////
//class error_logLL
//Constructor
error_logLL::error_logLL(dNdQ2_data *dist_Events){
	this->dist_Events=dist_Events;
}
//Destructor
error_logLL::~error_logLL() = default;

//Method
double error_logLL::error(int BIN, double precission, int iter){
	this->dist_Events->distEvents(precission, iter);
	int i;
	double error=0.0;
	double delta;
	for(i=BIN;i<this->dist_Events->nbins;i++) {
		if(this->dist_Events->dNdQ2[i]>0){
			delta = 2.0*(this->dist_Events->th_dNdQ2[i]-this->dist_Events->dNdQ2[i]+this->dist_Events->dNdQ2[i]*log(this->dist_Events->dNdQ2[i]/this->dist_Events->th_dNdQ2[i]));
		} else {
			delta = 2.0*(this->dist_Events->th_dNdQ2[i]-this->dist_Events->dNdQ2[i]);
		}
		error+=delta;
	}
	return error;
}

void error_logLL::change_parameters(double new_parameters){
	this->dist_Events->Nfit=new_parameters;
}

/////////////////////////////////////////////////////////////////////
//class error_chi2_dsdQ2

//Constructor
error_chi2_dsdQ2::error_chi2_dsdQ2(dsdQ2_data *diff_cs){
	this->diff_cs=diff_cs;
// 	this->add_error_correction=add_error_correction;
// 	this->dn=dn;
}	
//Destructor
error_chi2_dsdQ2::~error_chi2_dsdQ2() = default;

//Method
double error_chi2_dsdQ2::error(int BIN, double precission, int iter){
	this->diff_cs->diffCS(precission, iter);
	int i;
	double error=0.0;
	double delta;
	for(i=BIN;i<this->diff_cs->nbins;i++) {
		delta=(this->diff_cs->th_dsdQ2[i]-this->diff_cs->dsdQ2[i])/this->diff_cs->var[i];
		
// 		cout<<this->diff_cs->th_dsdQ2[i]<<" "<<this->diff_cs->dsdQ2[i]<<" "<<this->diff_cs->var[i]<<" "<<delta<<endl;
		
		error+=delta*delta;
	}
// 	if (this->add_error_correction == true) error+=(1.0-this->diff_cs->Nfit)*(1.0-this->diff_cs->Nfit)/(this->dn*this->dn)+0.01851236399832205/(2.0*174.0);
	return error;
}
void error_chi2_dsdQ2::change_parameters(double new_parameters){
	this->diff_cs->Nfit=new_parameters;
}