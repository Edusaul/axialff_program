//
// Created by edusaul on 18/06/18.
//

#include "minuit_minimization_zExp.h"

//minuit_minimization::minuit_minimization(){}
//minuit_minimization::~minuit_minimization(){}
//double minuit_minimization::minimize(){}

minuit_minimization_zExp::minuit_minimization_zExp(int number_of_parameters, std::vector<bool> min_experiment,
                                                   bool use_Nfit, bool do_Minos) {

    this->number_of_parameters=number_of_parameters;
    this->min_experiment=min_experiment;
    this->use_Nfit=use_Nfit;
    this->do_Minos=do_Minos;

    this->par.resize(static_cast<unsigned long>(number_of_parameters));
    this->stepSize.resize(static_cast<unsigned long>(number_of_parameters));
    this->minVal.resize(static_cast<unsigned long>(number_of_parameters));
    this->maxVal.resize(static_cast<unsigned long>(number_of_parameters));
    this->parName.resize(static_cast<unsigned long>(number_of_parameters));
    this->out_par.resize(static_cast<unsigned long>(number_of_parameters));
    this->err_par.resize(static_cast<unsigned long>(number_of_parameters));

    for (int i = 0; i < Param::number_of_a_zExp; ++i) {
        this->par[i] = 1.0;            // a guess
		this->stepSize[i] = 0.1;       // take e.g. 0.1 of start value
		this->minVal[i] = 0;   // if min and max values = 0, parameter is unbounded.
		this->maxVal[i] = 0;
		this->parName[i] = "a" + to_string(i+1);
    }

    for (int i = Param::number_of_a_zExp; i < number_of_parameters; ++i) {
        this->par[i] = 1.0;            // a guess
        this->stepSize[i] = 0.1;       // take e.g. 0.1 of start value
        this->minVal[i] = 0.01;   // if min and max values = 0, parameter is unbounded.
        this->maxVal[i] = 100;
        this->parName[i] = "Nfit_" + to_string(i-Param::number_of_a_zExp);
    }
}

double minuit_minimization_zExp::minimize() {
    TMinuit minuit(number_of_parameters);
    minuit.SetFCN(fcn_zExp);

    for (int i=0; i<this->number_of_parameters; i++){
    minuit.DefineParameter(i, this->parName[i].c_str(),
      this->par[i], this->stepSize[i], this->minVal[i], this->maxVal[i]);
	}

	double outpar[this->number_of_parameters], err[this->number_of_parameters];

    double arglist[10];
	int ierflg=0;
	arglist[0]=0;
	minuit.mnexcm("MIGRAD",arglist,0,ierflg);
	if(do_Minos) minuit.mnexcm("MINOS", arglist, 0, ierflg);

    for (int i=0; i<this->number_of_parameters; i++){
		minuit.GetParameter(i,outpar[i],err[i]);
	}

	for(int i=0; i<this->number_of_parameters; i++){
		this->out_par[i]=outpar[i];
		this->err_par[i]=err[i];
	}

    int j=Param::number_of_a_zExp;
	for (int i=0;i<Param::n_of_dist;i++) {
		if(min_experiment[i]){
			cout<<"	"<<this->parName[j]<<" = "<<outpar[j]<<" ± "<<err[j]<<endl;
			j++;
		}
	}
	return 0;
}

///////////////////////////////////////////////////////////////////
// function to do the minimization for a dipole

void fcn_zExp(int &npar, double *deriv, double &f, double par[], int flag) {
    int j=Param::number_of_a_zExp;
	double error=0.0;

	double gaussian_boud_aka0_bound = 0.;
	double bound = 6.;
	double sigma = 1.;
	double regularizer=10.0;

	std::vector<double> Param_a_zExp(static_cast<unsigned long>(Param::number_of_a_zExp));
    for (int i = 0; i < Param::number_of_a_zExp; ++i) {
        Param_a_zExp[i] = par[i];
        cout<< std::right <<std::setw(5) <<"a"<<i+1<<" = "<< std::setw(8) <<Param_a_zExp[i]<< std::left<<std::setw(4) <<" ,";

//        if(i>0) gaussian_boud_aka0_bound += exp(-0.5 * ((Param_a_zExp[i]/Param_a_zExp[0] - bound)/sigma)
//        		*((abs(Param_a_zExp[i]/Param_a_zExp[0]) - bound)/sigma))/(sigma * sqrt(2.*Param::pi));
//        if(i>0) gaussian_boud_aka0_bound += exp(-0.5 * ((Param_a_zExp[i]/Param_a_zExp[0])/bound)
//                                                *((Param_a_zExp[i]/Param_a_zExp[0])/bound))/(sigma * sqrt(2.*Param::pi));
        if(i>0) gaussian_boud_aka0_bound += 0.5 * ((Param_a_zExp[i]/Param_a_zExp[0])/bound)
                                                *((Param_a_zExp[i]/Param_a_zExp[0])/bound);

    }
    FA_pointer->Change_other_imputs(Param_a_zExp);

    int k = 1;
    double Param_normalization;
	for (int i=0;i<Param::n_of_dist;i++){
		if(min_experiment[i]==true){
			Param_normalization=par[j];
			Array_of_classes[i]->change_parameters(Param_normalization);
			error+=Array_of_classes[i]->error(Param::BIN,1e-3,10);
			cout<< std::right <<std::setw(5) <<"Nfit_"<<k<<" = "<< std::setw(8) <<Param_normalization<< std::left<<std::setw(4) <<" ,";
			j++;
			k++;
		}
	}
	f=error + regularizer*gaussian_boud_aka0_bound;
	cout<< std::right<<"error function = "<<std::setw(7)<<error<<endl;
}