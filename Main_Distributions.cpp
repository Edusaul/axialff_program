//
// Created by edusaul on 12/06/18.
//

#include <ctime>
#include <Parameters_MeV.h>
#include <tclap/CmdLine.h>
#include "Flux_data.h"
#include "Form_Factors/FFV.h"
#include "Form_Factors/FA_dipole.h"
#include "Form_Factors/FA_1_monopole.h"
#include "Form_Factors/FA_2_monopoles.h"
#include "Form_Factors/FA_tripole.h"
#include "Form_Factors/KNN_output.h"
#include "Event_Distributions/dsigmadQ2.h"
#include "Event_Distributions/sigma_tot.h"
#include "Flux_class.h"
#include "Fluxfit.h"
#include "Event_Distributions/dNdQ2dEnu.h"
#include "Event_Distributions/dsigdQ2dEnu.h"
#include "write_class.h"
#include "Form_Factors/FA_z_exp.h"
#include "Write_FA_zExp.h"

using namespace TCLAP;
using namespace std;
using namespace Flux_data;

template <typename T>
std::string to_string(T value)
{
    std::ostringstream os ;
    os << value ;
    return os.str() ;
}

//Main program
int main(int argc, char** argv)
{
    clock_t time1, time2;
    time1 = clock(); //Start timer

    // Wrap everything in a try block.  Do this every time,
    // because exceptions will be thrown for problems.
    try {

/////////////////////////////////////////////////////////////////
        //Arguments of Main

        TCLAP::CmdLine cmd("Command description message", ' ', "0.1");

        // Define a value argument and add it to the command line.
        vector<int> allowed = {0,1,2};
        ValuesConstraint<int> allowedVals( allowed );

        ValueArg<int> choiceSinghANL_Arg("","ASingh","Singh correction for ANL; 0 -> off, 1 -> parameter outside integrals, "
                                                     "2 (default) -> Inside all cross section calculations (including integrated one)",false, 2, &allowedVals);
        cmd.add( choiceSinghANL_Arg );
        ValueArg<int> choiceSinghBNL_Arg("","BSingh","Singh correction for BNL; 0 -> off, 1 -> parameter outside integrals, "
                                                     "1 (default) -> Inside all cross section calculations (including integrated one)",false, 1, &allowedVals);
        cmd.add( choiceSinghBNL_Arg );
        ValueArg<int> choiceSinghFNAL_Arg("","FSingh","Singh correction for FNAL; 0 -> off, 1 -> parameter outside integrals, "
                                                     "2 (default) -> Inside all cross section calculations (including integrated one)",false, 2, &allowedVals);
        cmd.add( choiceSinghFNAL_Arg );
        ValueArg<int> choiceSinghBEBC_Arg("","BESingh","Singh correction for BEBC; 0 -> off, 1 -> parameter outside integrals, "
                                                      "2 (default) -> Inside all cross section calculations (including integrated one)",false, 2, &allowedVals);
        cmd.add( choiceSinghBEBC_Arg );

        SwitchArg NNSwitch("N","useNNout","Use Neural Net output for FA", false);
        SwitchArg dipole_Switch("d","dipole","Use monopole for FA", false);
        SwitchArg one_monopole_Switch("","monopole","Use monopole for FA", false);
        SwitchArg two_monopoles_Switch("","twoMonopoles","Use two monopoles for FA", false);
        SwitchArg tripole_Switch("","tripole","Use tripole for FA", false);
        SwitchArg z_exp_Switch("z","zExp","Use z expansion for FA", false);
        vector<Arg*>  xorlist;
        xorlist.push_back(&NNSwitch);
        xorlist.push_back(&dipole_Switch);
        xorlist.push_back(&one_monopole_Switch);
        xorlist.push_back(&two_monopoles_Switch);
        xorlist.push_back(&tripole_Switch);
        xorlist.push_back(&z_exp_Switch);
        cmd.xorAdd( xorlist );

        SwitchArg No_error_bandsSwitch("","NOerror","Do not plot the error bands", true);
        cmd.add( No_error_bandsSwitch );

        ValueArg<double> ANL_Nfit_Arg("A","ANLNfit","Set the normalization constant for ANL",false, 1.0938, "double");
        cmd.add( ANL_Nfit_Arg );
        ValueArg<double> BNL_Nfit_Arg("B","BNLNfit","Set the normalization constant for BNL",false, 1.12, "double");
        cmd.add( BNL_Nfit_Arg );
        ValueArg<double> FNAL_Nfit_Arg("F","FNALNfit","Set the normalization constant for FNAL",false, 1.09, "double");
        cmd.add( FNAL_Nfit_Arg );
        ValueArg<double> BEBC_Nfit_Arg("","BEBCNfit","Set the normalization constant for BEBC",false, 0.65, "double");
        cmd.add( BEBC_Nfit_Arg );

        ValueArg<double> MA_Arg("","MA","Set the value for the axial mass",false, 1045.5, "double");
        cmd.add( MA_Arg );

        ValueArg<string> ANL_path_Arg("","ANLPath","Set the path to write the ANL distribution file",false, "../Data/dsdpK_ANL.dat", "string");
        cmd.add( ANL_path_Arg );
        ValueArg<string> BNL_path_Arg("","BNLPath","Set the path to write the BNL distribution file",false, "../Data/dsdpK_BNL.dat", "string");
        cmd.add( BNL_path_Arg );
        ValueArg<string> FNAL_path_Arg("","FNALPath","Set the path to write the FNAL distribution file",false, "../Data/dsdpK_FNAL.dat", "string");
        cmd.add( FNAL_path_Arg );
        ValueArg<string> BEBC_path_Arg("","BEBCPath","Set the path to write the BEBC distribution file",false, "../Data/dsdpK_BEBC.dat", "string");
        cmd.add( BEBC_path_Arg );
        ValueArg<string> FA_path_Arg("","FAPath","Set the path to write the FA file",false, "../Data/FA.dat", "string");
        cmd.add( FA_path_Arg );

        ValueArg<string> FA_plot_path_Arg("","FAPlot","Set the path to plot the FA",false, "../Plots/FA.dat", "string");
        cmd.add( FA_plot_path_Arg );

        // Parse the args.
        cmd.parse( argc, argv );

/////////////////////////////////////////////////////////////////////////
        // path to write the distributions

        std::string FF_file_ANL_path=ANL_path_Arg.getValue(); //path for opening output document
        std::string FF_file_BNL_path=BNL_path_Arg.getValue(); //path for opening output document
        std::string FF_file_FNAL_path=FNAL_path_Arg.getValue(); //path for opening output document
        std::string FF_file_BEBC_path=BEBC_path_Arg.getValue(); //path for opening output document
        std::string FF_file_FA_path=FA_path_Arg.getValue(); //path for opening output document

        std::string Plot_FA_path=FA_plot_path_Arg.getValue(); //path for opening output document

/////////////////////////////////////////////////////////////////////////

        bool one_monopole = one_monopole_Switch.getValue();
        bool two_monopoles = two_monopoles_Switch.getValue();
        bool tripole = tripole_Switch.getValue();
        bool use_NN_out = NNSwitch.getValue();
        bool z_exp = z_exp_Switch.getValue();

        bool use_Nfit(Param::n_of_dist);
        use_Nfit=true; //if false N_fit will be constant in minuit minimization

        //Choice for Singh correction: 0 -> off, 1 -> parameter outside integrals,
        // 2 -> Inside all cross section calculations (including integrated one)
        int choiceSinghANL = choiceSinghANL_Arg.getValue();
        int choiceSinghBNL = choiceSinghBNL_Arg.getValue();
        int choiceSinghFNAL = choiceSinghFNAL_Arg.getValue();
        int choiceSinghBEBC = choiceSinghBEBC_Arg.getValue();

        bool error_bands=No_error_bandsSwitch.getValue();

/////////////////////////////////////////////////////////////////////////


        // Axial mass and incident neutrino energy to write distributions
        std::vector<double> MA(2);
        std::vector<double> Delta_MA_plus(2);
        std::vector<double> Delta_MA_minus(2);
// 	MA[0]=600.0; // 1pole
        MA[0]=MA_Arg.getValue();
// 	MA[0]=1435.26; // 3pole
        MA[1]=100.0;
        Delta_MA_minus[0]=60.0;
        Delta_MA_minus[1]=0.0;
        Delta_MA_plus[0]=60.0;
        Delta_MA_plus[1]=0.0;

        double ANL_Nfit=ANL_Nfit_Arg.getValue();//Nfit;
        double BNL_Nfit=BNL_Nfit_Arg.getValue();//Nfit;
        double FNAL_Nfit=FNAL_Nfit_Arg.getValue();//Nfit;
        double BEBC_Nfit=BEBC_Nfit_Arg.getValue();//Nfit;

/////////////////////////////////////////////////////////////////////////

// Declaration of classes
        FFV FF_V(1);
// 	KNN_FV FF_V(1);
        FA_dipole FA_dip;
        FA_1_monopole FA_1_mono;
        FA_2_monopoles FA_monopoles;
        FA_tripole FA_3pole;
        FA_z_exp FA_z;
        Call_Function_Base *FA_pointer;

        FA_pointer=&FA_dip;
        if(one_monopole) FA_pointer=&FA_1_mono;
        if(two_monopoles) FA_pointer=&FA_monopoles;
        if(tripole) FA_pointer=&FA_3pole;
        if(z_exp)  FA_pointer=&FA_z;
        std::vector<double> z_exp_a = {2.3,-0.6,-3.8,2.3};
        if(z_exp) MA = z_exp_a;

        KNN_FA_NEW KNN;
        if(use_NN_out) {
            FA_pointer=&KNN;
        }
        dsigmadQ2 dsdQ2(&FF_V,FA_pointer,false);
        Integration_Class dsdQ2_integral(&dsdQ2);
        sigma_tot sigma(&dsdQ2_integral);


        flux_class ANL_flux(ANL_path,ANL_nbins,&flx_fit::fluxfit_ANL,ANL_dimensions,ANL_usefluxfit);
        flux_class BNL_flux(BNL_path,BNL_nbins,&flx_fit::fluxfit_BNL,BNL_dimensions,BNL_usefluxfit);
        flux_class FNAL_flux(FNAL_path,FNAL_nbins,&flx_fit::fluxfit_FNAL,FNAL_dimensions,FNAL_usefluxfit);

        flux_class BEBC_flux(BEBC_path,BEBC_nbins,&flx_fit::fluxfit_BEBC,BEBC_dimensions,BEBC_usefluxfit);

        dNdQ2dEnu dN_ANL(&dsdQ2,&ANL_flux,&sigma,choiceSinghANL);
        dNdQ2dEnu dN_BNL(&dsdQ2,&BNL_flux,&sigma,choiceSinghBNL);
        dNdQ2dEnu dN_FNAL(&dsdQ2,&FNAL_flux,&sigma,choiceSinghFNAL);

        dsigdQ2dEnu ds_BEBC(&dsdQ2,&BEBC_flux,&sigma,choiceSinghBEBC);

        Integration_Class Nth_ANL(&dN_ANL);
        Integration_Class Nth_BNL(&dN_BNL);
        Integration_Class Nth_FNAL(&dN_FNAL);
        Integration_Class int_flux_ANL(&ANL_flux);
        Integration_Class int_flux_BNL(&BNL_flux);
        Integration_Class int_flux_FNAL(&FNAL_flux);

        Integration_Class Sig_th_BEBC(&ds_BEBC);
        Integration_Class int_flux_BEBC(&BEBC_flux);

/////////////////////////////////////////////////////////////////////////
        // Integration of flux for normalization
        double ANL_total_flux=int_flux_ANL.Integrate(ANL_Enumin,ANL_Enumax,1e-1);
        double BNL_total_flux=int_flux_BNL.Integrate(BNL_Enumin,BNL_Enumax,1e-1);
        double FNAL_total_flux=int_flux_FNAL.Integrate(FNAL_Enumin,FNAL_Enumax,1e-1);

        double BEBC_total_flux=int_flux_BEBC.Integrate(BEBC_Enumin,BEBC_Enumax,1e-1);
/////////////////////////////////////////////////////////////////////////
        // Declaration of the class which calculate the event distribution
        dNdQ2_data ANL_distribution(ANL_data_path,ANL_data_nbins,ANL_Nfit,ANL_data_tot,ANL_total_flux,ANL_data_EnuAv,ANL_Enumin,ANL_Enumax,&Nth_ANL);
        dNdQ2_data BNL_distribution(BNL_data_path,BNL_data_nbins,BNL_Nfit,BNL_data_tot,BNL_total_flux,BNL_data_EnuAv,BNL_Enumin,BNL_Enumax,&Nth_BNL);
        dNdQ2_data FNAL_distribution(FNAL_data_path,FNAL_data_nbins,FNAL_Nfit,FNAL_data_tot,FNAL_total_flux,FNAL_data_EnuAv,FNAL_Enumin,FNAL_Enumax,&Nth_FNAL);

        dsdQ2_data BEBC_diff_cs_avg(BEBC_data_path,BEBC_data_nbins,BEBC_Nfit,BEBC_bin_div,BEBC_total_flux,BEBC_Enumin,BEBC_Enumax,lep_momentum_cut,BEBC_data_factor,&Sig_th_BEBC);

        // Writing event distribution
        write_distribution write_ANL(&ANL_distribution, FF_file_ANL_path, FA_pointer, MA, Delta_MA_plus, Delta_MA_minus, error_bands);
        write_distribution write_BNL(&BNL_distribution, FF_file_BNL_path, FA_pointer, MA, Delta_MA_plus, Delta_MA_minus, error_bands);
        write_distribution write_FNAL(&FNAL_distribution, FF_file_FNAL_path, FA_pointer, MA, Delta_MA_plus, Delta_MA_minus, error_bands);
        write_distribution_dsdQ2 write_BEBC(&BEBC_diff_cs_avg, FF_file_BEBC_path, FA_pointer, MA, Delta_MA_plus, Delta_MA_minus, error_bands);

        write_ANL.write();
        write_BNL.write();
        write_FNAL.write();
        write_BEBC.write();
        FA_pointer=&FA_z;
        if(z_exp) {
            Write_FA_zExp write_FA(FF_file_FA_path, Plot_FA_path, FA_pointer, error_bands);
            write_FA.write(0.0, 3e6, 50);
        }else {
            write_FA write_FA(FF_file_FA_path, FA_pointer, MA, Delta_MA_plus, Delta_MA_minus, error_bands);
            write_FA.write(0.0, 3e6, 50);
        }

    } catch (ArgException &e)  // catch any exceptions
	{ cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }

/////////////////////////////////////////////////////////////////////////

    time2= clock(); //timer
    float diff;
    diff= ((float)time2-(float)time1)/CLOCKS_PER_SEC;
    cout<<endl<<"Time"<<" "<<diff<<" "<<"s"<<endl;

    return 0;

}