//
// Created by eduardo on 20/06/18.
//

#include <ctime>
#include <Parameters_MeV.h>
#include <sstream>
#include "Minuit/Error_class.h"
#include "Flux_data.h"
#include "Form_Factors/KNN_output.h"
#include "Fluxfit.h"
#include "write_class.h"
#include "Form_Factors/FA_dipole.h"
#include "Form_Factors/FA_1_monopole.h"
#include "Form_Factors/FA_2_monopoles.h"
#include "Form_Factors/FA_tripole.h"
#include "Event_Distributions/dsigmadQ2.h"
#include "Event_Distributions/sigma_tot.h"
#include "Event_Distributions/dsigdQ2dEnu.h"
#include "Flux_class.h"
#include "Form_Factors/FA_z_exp.h"
#include <tclap/CmdLine.h>
#include <Call_Function.h>
#include <Integration.h>

using namespace TCLAP;
using namespace std;
using namespace Flux_data;

calc_error *Array_of_classes[Param::n_of_dist];
Call_Function_Base *FA_pointer;

//Main program
int main(int argc, char** argv)
{
    clock_t time1, time2;
    time1 = clock(); //Start timer

    // Wrap everything in a try block.  Do this every time,
    // because exceptions will be thrown for problems.
    try {

        /////////////////////////////////////////////////////////////////
        //Arguments of Main

        TCLAP::CmdLine cmd("Command description message", ' ', "0.1");

        // Define a value argument and add it to the command line.
        vector<int> allowed = {0,1,2};
        ValuesConstraint<int> allowedVals( allowed );
        ValueArg<int> choiceSinghANL_Arg("","ASingh","Singh correction for ANL; 0 -> off, 1 -> parameter outside integrals, "
                                                     "2 (default) -> Inside all cross section calculations (including integrated one)",false, 2, &allowedVals);
        cmd.add( choiceSinghANL_Arg );
        ValueArg<int> choiceSinghBNL_Arg("","BSingh","Singh correction for BNL; 0 -> off, 1 -> parameter outside integrals, "
                                                     "1 (default) -> Inside all cross section calculations (including integrated one)",false, 1, &allowedVals);
        cmd.add( choiceSinghBNL_Arg );
        ValueArg<int> choiceSinghFNAL_Arg("","FSingh","Singh correction for FNAL; 0 -> off, 1 -> parameter outside integrals, "
                                                      "2 (default) -> Inside all cross section calculations (including integrated one)",false, 2, &allowedVals);
        cmd.add( choiceSinghFNAL_Arg );
        ValueArg<int> choiceSinghBEBC_Arg("","BESingh","Singh correction for BEBC; 0 -> off, 1 -> parameter outside integrals, "
                                                       "2 (default) -> Inside all cross section calculations (including integrated one)",false, 2, &allowedVals);
        cmd.add( choiceSinghBEBC_Arg );

        ValueArg<int> binArg("","bin","Beginning bin, default = 0",false, 0, "int");
        cmd.add( binArg );

//        ValueArg<int> number_of_a_zExp_Arg("","NazExp","Number of a parameters for z expansion, default = 4",false, 4, "int");
//        cmd.add( number_of_a_zExp_Arg );

        // Define a switch and add it to the command line.

        SwitchArg error_correction_Switch("e","noErrorCorrection","Don't use error correction", false);
        cmd.add( error_correction_Switch );

        SwitchArg logLL_Switch("l","logLL","Use -2 log likelihood error function", false);
        cmd.add( logLL_Switch );

//        SwitchArg ANL_Switch("A","ANL","DON'T minuit with ANL", false);
//        cmd.add( ANL_Switch );
//        SwitchArg BNL_Switch("B","BNL","Do minuit with BNL", false);
//        cmd.add( BNL_Switch );
//        SwitchArg FNAL_Switch("F","FNAL","Do minuit with FNAL", false);
//        cmd.add( FNAL_Switch );
//        SwitchArg BEBC_Switch("","BEBC","Do minuit with BEBC", false);
//        cmd.add( BEBC_Switch );

        SwitchArg NNSwitch("N","useNNout","Use Neural Net output for FA", false);
        SwitchArg dipole_Switch("d","dipole","Use monopole for FA", false);
        SwitchArg one_monopole_Switch("","monopole","Use monopole for FA", false);
        SwitchArg two_monopoles_Switch("","twoMonopoles","Use two monopoles for FA", false);
        SwitchArg tripole_Switch("","tripole","Use tripole for FA", false);
        SwitchArg z_exp_Switch("z","zExp","Use z expansion for FA", false);
        vector<Arg*>  xorlist;
        xorlist.push_back(&NNSwitch);
        xorlist.push_back(&dipole_Switch);
        xorlist.push_back(&one_monopole_Switch);
        xorlist.push_back(&two_monopoles_Switch);
        xorlist.push_back(&tripole_Switch);
        xorlist.push_back(&z_exp_Switch);
        cmd.xorAdd( xorlist );

        ValueArg<double> ANL_Nfit_Arg("A","ANLNfit","Set the normalization constant for ANL",false, 1.0938, "double");
        cmd.add( ANL_Nfit_Arg );
        ValueArg<double> BNL_Nfit_Arg("B","BNLNfit","Set the normalization constant for BNL",false, 1.12, "double");
        cmd.add( BNL_Nfit_Arg );
        ValueArg<double> FNAL_Nfit_Arg("F","FNALNfit","Set the normalization constant for FNAL",false, 1.09, "double");
        cmd.add( FNAL_Nfit_Arg );
        ValueArg<double> BEBC_Nfit_Arg("","BEBCNfit","Set the normalization constant for BEBC",false, 0.65, "double");
        cmd.add( BEBC_Nfit_Arg );

        ValueArg<double> MA_Arg("","MA","Set the value for the axial mass",false, 1045.51, "double");
        cmd.add( MA_Arg );

        // Parse the args.
        cmd.parse( argc, argv );

        /////////////////////////////////////////////////////////////////


        // Activate (true) / deactivate (false) use NN output
        bool use_NN_out = NNSwitch.getValue();

        bool add_error_correction = true;
        if (error_correction_Switch.getValue()) add_error_correction = false;
        int BIN = binArg.getValue(); // don't use the first BIN bins in the error calculation

//        bool dipole = dipole_Switch.getValue();
        bool one_monopole = one_monopole_Switch.getValue();
        bool two_monopoles = two_monopoles_Switch.getValue();
        bool tripole = tripole_Switch.getValue();
        bool z_exp = z_exp_Switch.getValue();

        bool logLL_error_function = logLL_Switch.getValue();


        //Choice for Singh correction: 0 -> off, 1 -> parameter outside integrals,
        // 2 -> Inside all cross section calculations (including integrated one)
        int choiceSinghANL = choiceSinghANL_Arg.getValue();
        int choiceSinghBNL = choiceSinghBNL_Arg.getValue();
        int choiceSinghFNAL = choiceSinghFNAL_Arg.getValue();
        int choiceSinghBEBC = choiceSinghBEBC_Arg.getValue();


/////////////////////////////////////////////////////////////////////////
        // Axial mass and incident neutrino energy to write distributions
        std::vector<double> MA(2);
        std::vector<double> Delta_MA_plus(2);
        std::vector<double> Delta_MA_minus(2);
// 	MA[0]=600.0; // 1pole
        MA[0]=MA_Arg.getValue();
// 	MA[0]=1435.26; // 3pole
        MA[1]=100.0;
        Delta_MA_minus[0]=60.0;
        Delta_MA_minus[1]=0.0;
        Delta_MA_plus[0]=60.0;
        Delta_MA_plus[1]=0.0;

        double ANL_Nfit=ANL_Nfit_Arg.getValue();//Nfit;
        double BNL_Nfit=BNL_Nfit_Arg.getValue();//Nfit;
        double FNAL_Nfit=FNAL_Nfit_Arg.getValue();//Nfit;
        double BEBC_Nfit=BEBC_Nfit_Arg.getValue();//Nfit;
        double ANL_dn = 0.2; // normalization constant error
        double BNL_dn = 0.2; // normalization constant error
        double FNAL_dn = 0.2; // normalization constant error

        // Declaration of classes
        FFV FF_V(1);
// 	KNN_FV FF_V(1);
        FA_dipole FA_dip;
        FA_1_monopole FA_1_mono;
        FA_2_monopoles FA_monopoles;
        FA_tripole FA_3pole;
        FA_z_exp FA_z;

        FA_pointer = &FA_dip;
        if (one_monopole) FA_pointer = &FA_1_mono;
        if (two_monopoles) FA_pointer = &FA_monopoles;
        if (tripole) FA_pointer = &FA_3pole;
        if (z_exp)  FA_pointer=&FA_z;
        std::vector<double> z_exp_a = {2.3,-0.6,-3.8,2.3};
        if(z_exp) MA = z_exp_a;

        KNN_FA_NEW KNN;
        if (use_NN_out) FA_pointer = &KNN;

        dsigmadQ2 dsdQ2(&FF_V, FA_pointer, false);
        Integration_Class dsdQ2_integral(&dsdQ2);
        sigma_tot sigma(&dsdQ2_integral);


        flux_class ANL_flux(ANL_path, ANL_nbins, &flx_fit::fluxfit_ANL, ANL_dimensions, ANL_usefluxfit);
        flux_class BNL_flux(BNL_path, BNL_nbins, &flx_fit::fluxfit_BNL, BNL_dimensions, BNL_usefluxfit);
        flux_class FNAL_flux(FNAL_path, FNAL_nbins, &flx_fit::fluxfit_FNAL, FNAL_dimensions, FNAL_usefluxfit);

        flux_class BEBC_flux(BEBC_path, BEBC_nbins, &flx_fit::fluxfit_BEBC, BEBC_dimensions, BEBC_usefluxfit);

        dNdQ2dEnu dN_ANL(&dsdQ2, &ANL_flux, &sigma, choiceSinghANL);
        dNdQ2dEnu dN_BNL(&dsdQ2, &BNL_flux, &sigma, choiceSinghBNL);
        dNdQ2dEnu dN_FNAL(&dsdQ2, &FNAL_flux, &sigma, choiceSinghFNAL);

        dsigdQ2dEnu ds_BEBC(&dsdQ2, &BEBC_flux, &sigma, choiceSinghBEBC);

        Integration_Class Nth_ANL(&dN_ANL);
        Integration_Class Nth_BNL(&dN_BNL);
        Integration_Class Nth_FNAL(&dN_FNAL);
        Integration_Class int_flux_ANL(&ANL_flux);
        Integration_Class int_flux_BNL(&BNL_flux);
        Integration_Class int_flux_FNAL(&FNAL_flux);

        Integration_Class Sig_th_BEBC(&ds_BEBC);
        Integration_Class int_flux_BEBC(&BEBC_flux);

        /////////////////////////////////////////////////////////////////////////
        // Integration of flux for normalization
        double ANL_total_flux = int_flux_ANL.Integrate(ANL_Enumin, ANL_Enumax, 1e-1);
        double BNL_total_flux = int_flux_BNL.Integrate(BNL_Enumin, BNL_Enumax, 1e-1);
        double FNAL_total_flux = int_flux_FNAL.Integrate(FNAL_Enumin, FNAL_Enumax, 1e-1);
        double BEBC_total_flux = int_flux_BEBC.Integrate(BEBC_Enumin, BEBC_Enumax, 1e-1);

/////////////////////////////////////////////////////////////////////////
        // Declaration of the class which calculate the event distribution
        dNdQ2_data ANL_distribution(ANL_data_path, ANL_data_nbins, ANL_Nfit, ANL_data_tot, ANL_total_flux,
                                    ANL_data_EnuAv, ANL_Enumin, ANL_Enumax, &Nth_ANL);
        dNdQ2_data BNL_distribution(BNL_data_path, BNL_data_nbins, BNL_Nfit, BNL_data_tot, BNL_total_flux,
                                    BNL_data_EnuAv, BNL_Enumin, BNL_Enumax, &Nth_BNL);
        dNdQ2_data FNAL_distribution(FNAL_data_path, FNAL_data_nbins, FNAL_Nfit, FNAL_data_tot, FNAL_total_flux,
                                     FNAL_data_EnuAv, FNAL_Enumin, FNAL_Enumax, &Nth_FNAL);
        dsdQ2_data BEBC_diff_cs_avg(BEBC_data_path, BEBC_data_nbins, BEBC_Nfit, BEBC_bin_div, BEBC_total_flux,
                                    BEBC_Enumin, BEBC_Enumax, lep_momentum_cut, BEBC_data_factor, &Sig_th_BEBC);

        // Calculating error for the distribution

        error_chi2 er_ANL_chi2(&ANL_distribution, ANL_dn, add_error_correction);
        error_chi2 er_BNL_chi2(&BNL_distribution, BNL_dn, add_error_correction);
        error_chi2 er_FNAL_chi2(&FNAL_distribution, FNAL_dn, add_error_correction);

        error_logLL er_ANL_logLL(&ANL_distribution);
        error_logLL er_BNL_logLL(&BNL_distribution);
        error_logLL er_FNAL_logLL(&FNAL_distribution);

        calc_error *er_ANL = &er_ANL_chi2;
        calc_error *er_BNL = &er_BNL_chi2;
        calc_error *er_FNAL = &er_FNAL_chi2;

        error_chi2_dsdQ2 er_BEBC_chi2(&BEBC_diff_cs_avg);
        calc_error *er_BEBC = &er_BEBC_chi2;

        if (logLL_error_function) {
            er_ANL = &er_ANL_logLL;
            er_BNL = &er_BNL_logLL;
            er_FNAL = &er_FNAL_logLL;
        }

        FA_pointer->Change_other_imputs(MA);

        cout<<"ChiSq for ANL =  "<<er_ANL->error(BIN,1e-3,10)<<endl;
        cout<<"ChiSq for BNL =  "<<er_BNL->error(BIN,1e-3,10)<<endl;
        cout<<"ChiSq for FNAL =  "<<er_FNAL->error(BIN,1e-3,10)<<endl;

        cout<<"ChiSq for BEBC =  "<<er_BEBC->error(BIN,1e-50,50)<<endl;

    } catch (ArgException &e)  // catch any exceptions
    { cerr << "error: " << e.error() << " for arg " << e.argId() << endl; }

/////////////////////////////////////////////////////////////////////////

    time2= clock(); //timer
    float diff;
    diff= ((float)time2-(float)time1)/CLOCKS_PER_SEC;
    cout<<endl<<"Time"<<" "<<diff<<" "<<"s"<<endl;

    return 0;

}