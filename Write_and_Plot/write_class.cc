#include <Parameters_MeV.h>
#include <iostream>
#include "write_class.h"


//Constructor
write_distribution::write_distribution(dNdQ2_data *distribution, std::string file, Call_Function_Base *FA_pointer, std::vector<double> MA, std::vector<double> Delta_MA_plus, std::vector<double> Delta_MA_minus, bool error_bands){
	this->distribution=distribution;
	this->file=file;
	this->FA_pointer=FA_pointer;
	this->MA=MA; //MeV
	this->Delta_MA_plus=Delta_MA_plus;
	this->Delta_MA_minus=Delta_MA_minus;
	this->error_bands=error_bands;
}
//Destructor
write_distribution::~write_distribution() = default;

//Method
int write_distribution::write(double precission, int iter){
	ofstream FF_file(this->file.c_str()); //opening output document	
	
	std::vector<double> dNdQ2;
	std::vector<double> dNdQ2_minus_Delta;
	std::vector<double> dNdQ2_plus_Delta;
	
	std::vector<double> axial_mass;
	axial_mass=this->MA;
	
	FA_pointer->Change_other_imputs(axial_mass);
	this->distribution->distEvents(precission, iter);
	dNdQ2=this->distribution->th_dNdQ2;

	if(error_bands){
		
		for(int j=0;j<2;j++) axial_mass[j]=this->MA[j]-this->Delta_MA_minus[j];		
		FA_pointer->Change_other_imputs(axial_mass);
		this->distribution->distEvents(precission, iter);
		dNdQ2_minus_Delta=this->distribution->th_dNdQ2;
		
		for(int j=0;j<2;j++) axial_mass[j]=this->MA[j]+this->Delta_MA_plus[j];
		FA_pointer->Change_other_imputs(axial_mass);
		this->distribution->distEvents(precission, iter);
		dNdQ2_plus_Delta=this->distribution->th_dNdQ2;
		
		for (int i=0;i<this->distribution->nbins;i++){
			FF_file<<(this->distribution->Q2[i]-this->distribution->sizeBin[i]/2.0)*1.0e-6<<" "<<(this->distribution->Q2[i]+this->distribution->sizeBin[i]/2.0)*1.0e-6<<" "<<dNdQ2_minus_Delta[i]<<" "<<dNdQ2[i]<<" "<<dNdQ2_plus_Delta[i]<<endl;
		}
	} else{	
		for (int i=0;i<this->distribution->nbins;i++){
			FF_file<<(this->distribution->Q2[i]-this->distribution->sizeBin[i]/2.0)*1.0e-6<<" "<<(this->distribution->Q2[i]+this->distribution->sizeBin[i]/2.0)*1.0e-6<<" "<<dNdQ2[i]<<endl;
		}
	}	
	
	return 0;
}


////////////////////////////////////////////////////////////////////////////

//Constructor
write_distribution_dsdQ2::write_distribution_dsdQ2(dsdQ2_data *distribution, std::string file, Call_Function_Base *FA_pointer, std::vector<double> MA, std::vector<double> Delta_MA_plus, std::vector<double> Delta_MA_minus, bool error_bands){
	this->distribution=distribution;
	this->file=file;
	this->FA_pointer=FA_pointer;
	this->MA=MA; //MeV
	this->Delta_MA_plus=Delta_MA_plus;
	this->Delta_MA_minus=Delta_MA_minus;
	this->error_bands=error_bands;
}
//Destructor
write_distribution_dsdQ2::~write_distribution_dsdQ2() = default;

//Method
int write_distribution_dsdQ2::write(double precission, int iter){
	ofstream FF_file(this->file.c_str()); //opening output document	
	
	std::vector<double> dNdQ2;
	std::vector<double> dNdQ2_minus_Delta;
	std::vector<double> dNdQ2_plus_Delta;
	
	std::vector<double> axial_mass(2);
	axial_mass=this->MA;
	
	FA_pointer->Change_other_imputs(axial_mass);
	this->distribution->diffCS(precission, iter);
	dNdQ2=this->distribution->th_dsdQ2;
	
	if(error_bands){
		for(int j=0;j<2;j++) axial_mass[j]=this->MA[j]-this->Delta_MA_minus[j];
		FA_pointer->Change_other_imputs(axial_mass);
		this->distribution->diffCS(precission, iter);
		dNdQ2_minus_Delta=this->distribution->th_dsdQ2;
		
		for(int j=0;j<2;j++) axial_mass[j]=this->MA[j]+this->Delta_MA_plus[j];
		FA_pointer->Change_other_imputs(axial_mass);
		this->distribution->diffCS(precission, iter);
		dNdQ2_plus_Delta=this->distribution->th_dsdQ2;
		
		for (int i=0;i<this->distribution->nbins;i++){
			FF_file<<(this->distribution->Q2[i]-this->distribution->sizeBin[i]/2.0)*1.0e-6<<" "<<(this->distribution->Q2[i]+this->distribution->sizeBin[i]/2.0)*1.0e-6<<" "<<dNdQ2_minus_Delta[i]<<" "<<dNdQ2[i]<<" "<<dNdQ2_plus_Delta[i]<<endl;
		}
	} else{	
		for (int i=0;i<this->distribution->nbins;i++){
			FF_file<<(this->distribution->Q2[i]-this->distribution->sizeBin[i]/2.0)*1.0e-6<<" "<<(this->distribution->Q2[i]+this->distribution->sizeBin[i]/2.0)*1.0e-6<<" "<<dNdQ2[i]<<endl;
		}
	}	
	
	return 0;
}

////////////////////////////////////////////////////////////////////////////



//Constructor
write_FA::write_FA(std::string file, Call_Function_Base *FA_pointer, std::vector<double> MA, std::vector<double> Delta_MA_plus, std::vector<double> Delta_MA_minus, bool error_bands){
	this->file=file;
	this->FA_pointer=FA_pointer;
	this->MA=MA; //MeV
	this->Delta_MA_plus=Delta_MA_plus;
	this->Delta_MA_minus=Delta_MA_minus;
	this->error_bands=error_bands;
}
//Destructor
write_FA::~write_FA() = default;

//Method
int write_FA::write(double Q2min, double Q2max, int Npts){
	ofstream FF_file(this->file.c_str()); //opening output document	
	
	std::vector<double> FA (Npts);
	std::vector<double> FA_minus_Delta (Npts);
	std::vector<double> FA_plus_Delta (Npts);
	
	std::vector<double> axial_mass(2);
	axial_mass=this->MA;
	
	this->FA_pointer->Change_other_imputs(axial_mass);
	
	double ga=Param::ga;
	
	std::vector<double> err (Npts);
	
	double Q2h=(Q2max-Q2min)/double(Npts-1);
	double Q2=Q2min;
	for(int i=0;i<Npts;i++){
		FA[i]=this->FA_pointer->Call_Fnc(Q2);
		err[i]=4.0*ga*Q2/(axial_mass[0]*axial_mass[0]*axial_mass[0]*(1.0+Q2/(axial_mass[0]*axial_mass[0]))*(1.0+Q2/(axial_mass[0]*axial_mass[0]))*(1.0+Q2/(axial_mass[0]*axial_mass[0])))*this->Delta_MA_plus[0];
		Q2 += Q2h;
	}
	
	if(error_bands){
		
		if(this->Delta_MA_plus != this->Delta_MA_minus){
			cout<<"Warning: the error of MA is not simmetric"<<endl;
		}
		
// 		for(int j=0;j<2;j++) axial_mass[j]=this->MA[j]-this->Delta_MA_minus[j];		
// 		this->FA_pointer->Change_other_imputs(axial_mass);
// 		Q2=Q2min;
// 		for(int i=0;i<Npts;i++){
// 			FA_minus_Delta[i]=this->FA_pointer->Call_Fnc(Q2);
// 			Q2 += Q2h;
// 		}
// 		
// 		for(int j=0;j<2;j++) axial_mass[j]=this->MA[j]+this->Delta_MA_plus[j];
// 		this->FA_pointer->Change_other_imputs(axial_mass);
// 		Q2=Q2min;
// 		for(int i=0;i<Npts;i++){
// 			FA_plus_Delta[i]=this->FA_pointer->Call_Fnc(Q2);
// 			Q2 += Q2h;
// 		}
		
// 		Q2=Q2min;
// 		for (int i=0;i<Npts;i++){
// 			cout<<Q2*1.0e-6<<"   "<<FA[i]<<"   "<<err[i]<<"   "<<FA[i]-FA_minus_Delta[i]<<"   "<<-FA[i]+FA_plus_Delta[i] <<"   "<<FA_minus_Delta[i]<<"   "<<FA_plus_Delta[i]<<endl;
// 			Q2 += Q2h;
// 		}
		
		Q2=Q2min;
		for (int i=0;i<Npts;i++){
			FF_file<<Q2*1.0e-6<<"   "<<FA[i]<<"   "<<err[i]<<"   "<<FA[i]-err[i]<<"   "<<FA[i]+err[i]<<endl;
			Q2 += Q2h;
		}
		
		
	} else{	
		Q2=Q2min;
		for (int i=0;i<Npts;i++){
			FF_file<<Q2*1.0e-6<<" "<<FA[i]<<endl;
			Q2 += Q2h;
		}
	}	
	
	return 0;
}


////////////////////////////////////////////////////////////////////////////
//Class write_FA_Bootstrap


//Constructor
write_FA_Bootstrap::write_FA_Bootstrap(std::string file, Call_Function_Base *FA_pointer, int N_minuits, std::vector<double> MA, std::vector<double> Delta_MA){
	this->file=file;
	this->FA_pointer=FA_pointer;
	this->MA=MA; //MeV
	this->N_minuits=N_minuits;
	this->Delta_MA.resize(static_cast<unsigned long>(this->N_minuits));
	this->Delta_MA=Delta_MA;
}
//Destructor
write_FA_Bootstrap::~write_FA_Bootstrap() = default;

//Method
int write_FA_Bootstrap::write(double Q2min, double Q2max, int Npts){
	ofstream FF_file(this->file.c_str()); //opening output document	
	
	std::vector<double> FA (static_cast<unsigned long>(Npts));
	std::vector<double> FA_minus_Delta (static_cast<unsigned long>(Npts));
	std::vector<double> FA_plus_Delta (static_cast<unsigned long>(Npts));
	
	std::vector<double> FA_Bootstrap_vector(static_cast<unsigned long>(this->N_minuits + 1));
	
	std::vector<double> axial_mass(2);
// 	axial_mass=this->MA;
	
// 	this->FA_pointer->Change_other_imputs(axial_mass);
	
// 	double ga=1.257;
	
	std::vector<double> FA_minus (static_cast<unsigned long>(Npts));
	std::vector<double> FA_plus (static_cast<unsigned long>(Npts));
	
	double Q2h=(Q2max-Q2min)/double(Npts-1);
	double Q2=Q2min;
	for(int i=0;i<Npts;i++){
		
		for(int j=0;j<this->N_minuits;j++){
			axial_mass[0]=this->MA[j];
			axial_mass[1]=this->Delta_MA[j];
			this->FA_pointer->Change_other_imputs(axial_mass);
			FA_Bootstrap_vector[j]=this->FA_pointer->Call_Fnc(Q2);
		}
		
		FA[i]=FA_Bootstrap_vector[0];
		std::sort(FA_Bootstrap_vector.begin(), FA_Bootstrap_vector.end());

		int nlow  = int(0.16*this->N_minuits);
		int nhigh = this->N_minuits - int(0.16*this->N_minuits);
		
		FA_minus[i]=FA_Bootstrap_vector[nlow];
		FA_plus[i]=FA_Bootstrap_vector[nhigh];
		
// 		FF_file<<Q2*1.0e-6<<"   "<<FA[i]<<" - "<<FA[i]-FA_minus[i]<<" +  "<<FA_plus[i]-FA[i]<<" ; "<<FA_minus[i]<<" < "<<FA[i]<<" < "<<FA_plus[i]<<endl;
		FF_file<<Q2*1.0e-6<<"   "<<FA[i]<<" "<<FA[i]-FA_minus[i]<<"  "<<FA_plus[i]-FA[i]<<" "<<FA_minus[i]<<" "<<FA[i]<<" "<<FA_plus[i]<<endl;
		
		Q2 += Q2h;
	}
	
	return 0;
}
