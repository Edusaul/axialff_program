//
// Created by edusaul on 17/06/18.
//
#include <iostream>
#include <fstream>
#include "Write_FA_zExp.h"


Write_FA_zExp::Write_FA_zExp(const std::string &file, const std::string &file_plot, Call_Function_Base *FA_pointer,
                             bool error_bands) : file(file), file_plot(file_plot), FA_pointer(FA_pointer),
                                                 error_bands(error_bands) {}

void Write_FA_zExp::write(double Q2min, double Q2max, int Npts) {
    std::ofstream FF_file(this->file.c_str()); //opening output document
    if (FF_file.is_open()) {
        std::vector<double> FA(Npts);
        std::vector<double> Q2_array(Npts);
        std::vector<std::vector<double>> data(2);

        double Q2h = (Q2max - Q2min) / double(Npts - 1);
        double Q2 = Q2min;
        for (int i = 0; i < Npts; i++) {
            FA[i] = this->FA_pointer->Call_Fnc(Q2);
            Q2_array[i] = Q2;
            Q2 += Q2h;
        }

        data[0] = Q2_array;
        data[1] = FA;

        for (int i = 0; i < Npts; i++) {
            FF_file << Q2_array[i] * 1.0e-6 << "   " << FA[i] << std::endl;
            Q2 += Q2h;
        }
    } else std::cout<<"ERROR : could not open file to write data"<<std::endl;

}