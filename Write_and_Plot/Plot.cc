#include "Plot.h"

//Plot Class

//Constructor
Plot::Plot(std::string file_path, std::string plot_path){
	this->file_path=file_path;
	this->plot_path=plot_path;
	this->do_Epilog=false;
}
//Destructor
Plot::~Plot() = default;

//Method
int Plot::Math_Plot(std::string AxesLabel, std::string PlotRange){
	
	std::string math;
	if(do_Epilog){
		math="wolframscript -code 'ListPlot[Import[\"" + this->file_path + "\",\"Data\"], AxesLabel->" + AxesLabel + ",Epilog -> {" + this->Epilog + "}, PlotRange->" + PlotRange + "]' -format PDF > " + this->plot_path;
	} else {
		math="wolframscript -code 'ListPlot[Import[\"" + this->file_path + "\",\"Data\"], AxesLabel->" + AxesLabel + ",PlotRange->" + PlotRange + "]' -format PDF > " + this->plot_path;
	}
	
	int aux = system(math.c_str());

	return 0;
}

int Plot::gnuplot_Plot(){
	std::string gnu="gnuplot -p -e \"plot '" + this->file_path + "'\"";
	int aux =system(gnu.c_str());
	return 0;
}

int Plot::add_Epilog(std::string Epilog){
	this->do_Epilog=true;
	this->Epilog=Epilog;
	return 0;
}

int Plot::Change_paths(std::string file_path, std::string plot_path){
	this->file_path=file_path;
	this->plot_path=plot_path;
}