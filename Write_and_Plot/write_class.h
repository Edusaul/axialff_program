#ifndef _WRITE_CLASS_H_
#define _WRITE_CLASS_H_

using namespace std;

#include <fstream>
#include "dNdQ2_data.h"
#include "dsdQ2_data.h"
#include <Call_Function.h>
#include <algorithm>    // std::sort

using namespace std;

class write_distribution {
protected:
	dNdQ2_data *distribution;
	std::string file;
	Call_Function_Base *FA_pointer;
	std::vector<double> MA;
	std::vector<double> Delta_MA_plus;
	std::vector<double> Delta_MA_minus;
	bool error_bands;
public:
	//Constructor
	write_distribution(dNdQ2_data*, std::string, Call_Function_Base*, std::vector<double>, std::vector<double>, std::vector<double>, bool error_bands=false);
	//Destructor
	virtual ~write_distribution();
	//Method
	int write(double precission=1e-3, int iter=10);
};

class write_distribution_dsdQ2 {
protected:
	dsdQ2_data *distribution;
	std::string file;
	Call_Function_Base *FA_pointer;
	std::vector<double> MA;
	std::vector<double> Delta_MA_plus;
	std::vector<double> Delta_MA_minus;
	bool error_bands;
public:
	//Constructor
	write_distribution_dsdQ2(dsdQ2_data*, std::string, Call_Function_Base*, std::vector<double>, std::vector<double>, std::vector<double>, bool error_bands=false);
	//Destructor
	virtual ~write_distribution_dsdQ2();
	//Method
	int write(double precission=1e-3, int iter=10);
};


class write_FA {
protected:
	std::string file;
	Call_Function_Base *FA_pointer;
	std::vector<double> MA;
	std::vector<double> Delta_MA_plus;
	std::vector<double> Delta_MA_minus;
	bool error_bands;
public:
	//Constructor
	write_FA(std::string, Call_Function_Base*, std::vector<double>, std::vector<double>, std::vector<double>, bool error_bands=false);
	//Destructor
	virtual ~write_FA();
	//Method
	int write(double, double, int Npts=35);
};


class write_FA_Bootstrap {
protected:
	std::string file;
	Call_Function_Base *FA_pointer;
	std::vector<double> MA;
	std::vector<double> Delta_MA; //just for 2 monopoles case
	int N_minuits;
public:
	//Constructor
	write_FA_Bootstrap(std::string, Call_Function_Base*,int, std::vector<double>, std::vector<double> Delta_MA= {});
	//Destructor
	virtual ~write_FA_Bootstrap();
	//Method
	int write(double, double, int Npts=35);
};

#endif