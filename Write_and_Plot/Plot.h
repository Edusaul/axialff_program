#ifndef PLOT_H
#define PLOT_H

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstdlib>

//Plot Class
class Plot {
protected:
	std::string file_path; //path for opening output
	std::string plot_path; //path for plot
	bool do_Epilog;
	std::string Epilog;
	
public:
	//Constructor
	Plot(std::string, std::string);
	//Destructor
	virtual ~Plot();
	//Method
	int Math_Plot(std::string AxesLabel="{}", std::string PlotRange="All"); 
	int gnuplot_Plot();
	int add_Epilog(std::string);
	int Change_paths(std::string, std::string);
};


#endif // PLOT_H