//
// Created by edusaul on 17/06/18.
//

#ifndef AXIALFF_WRITE_FA_ZEXP_H
#define AXIALFF_WRITE_FA_ZEXP_H


#include <Call_Function.h>

class Write_FA_zExp {
protected:
	std::string file;
    std::string file_plot;
	Call_Function_Base *FA_pointer;
	bool error_bands;

public:
    Write_FA_zExp(const std::string &file, const std::string &file_plot, Call_Function_Base *FA_pointer,
                  bool error_bands);

    void write(double, double, int Npts=35);
};


#endif //AXIALFF_WRITE_FA_ZEXP_H
