//
// Created by eduardo on 11/06/18.
//

#ifndef AXIALFF_DNDQ2_DATA_H
#define AXIALFF_DNDQ2_DATA_H

#include <vector>
#include <string>
#include <Integration.h>

using namespace std;

class dNdQ2_data {
public:
	string path;
	int nbins;
	double Nfit;
	vector<double> Q2;
	vector<double> dNdQ2;
	vector<double> var;
	vector<double> sizeBin;
	double total_events; //total number of events data
	double total_flux; //total number of events flux
	double EnuAv; //averaged energy of the flux
	double EnuMin; //for integratiion over the flux -> for normalization
	double EnuMax; //for integratiion over the flux -> for normalization
	Integration_Class *Nth; //Theoretical distribution
	vector<double> th_dNdQ2;

	//Constructor
	dNdQ2_data(string, int, double, double, double, double, double, double, Integration_Class*);
	//Destructor
	virtual ~dNdQ2_data();
	//Method
	virtual void distEvents(double precission=1e-3, int iter=10);
};

#endif //AXIALFF_DNDQ2_DATA_H
