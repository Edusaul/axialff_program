//
// Created by edusaul on 11/06/18.
//

#ifndef AXIALFF_CALL_FUNCTION_BASE_W_BOOL_H
#define AXIALFF_CALL_FUNCTION_BASE_W_BOOL_H

#include <Call_Function.h>

// Call_Function_Base with method to change bool parameter
class Call_Function_Base_w_bool : public Call_Function_Base {
protected:

public:
    //Constructor
    Call_Function_Base_w_bool();
    //Destructor
    ~Call_Function_Base_w_bool() override;
    //Method
    virtual void Change_bool_param(bool);
};


#endif //AXIALFF_CALL_FUNCTION_BASE_W_BOOL_H
