//
// Created by eduardo on 11/06/18.
//

#include <Parameters_MeV.h>
#include <iostream>
#include <fstream>
#include "dNdQ2_data.h"

//Destructor
dNdQ2_data::~dNdQ2_data() = default;

//Constructor
dNdQ2_data::dNdQ2_data(string path, int nbins, double Nfit, double total_events, double total_flux, double EnuAv, double EnuMin, double EnuMax, Integration_Class *Nth){
	this->path=path;
	this->nbins=nbins;
	this->EnuAv=EnuAv;
	this->EnuMin=EnuMin;
	this->EnuMax=EnuMax;
	this->Nth=Nth;
	this->Nfit=Nfit;
	this->total_events=total_events;
	this->total_flux=total_flux;

	ifstream file(path.c_str());//opening imput document
	if(!file) cout<<"WARNING! flux file \""<<path<<"\" not found"<<endl;
	int i;
	double dataX,dataY,dataZ,dataSB;
	this->Q2.resize(static_cast<unsigned long>(this->nbins));
	this->dNdQ2.resize(static_cast<unsigned long>(this->nbins));
	this->var.resize(static_cast<unsigned long>(this->nbins));
	this->sizeBin.resize(static_cast<unsigned long>(this->nbins));
	this->th_dNdQ2.resize(static_cast<unsigned long>(this->nbins));
	for (i=0;i<nbins;i++){
		file>>dataX;
		file>>dataY;
		file>>dataZ;
		file>>dataSB;
		this->Q2[i]=dataX*1.0e6;
		this->dNdQ2[i]=dataY;
		this->var[i]=dataZ;
		this->sizeBin[i]=dataSB*1.0e6;
	}
	file.close();


}

//Method
void dNdQ2_data::distEvents(double precission, int iter){
	double dNdQ2center;
	vector<double> parameters(1);
	int i;
	for (i=0;i<this->nbins;i++){
		parameters[0]=Q2[i];
		Nth->Change_other_imputs(parameters);
		dNdQ2center=Nth->Integrate(this->EnuMin,this->EnuMax,precission,iter)*this->Nfit/this->total_flux;
		this->th_dNdQ2[i]=dNdQ2center*this->sizeBin[i]*this->total_events;
	}
}