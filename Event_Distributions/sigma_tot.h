//
// Created by eduardo on 11/06/18.
//

#ifndef AXIALFF_SIGMA_TOT_H
#define AXIALFF_SIGMA_TOT_H

#include <Call_Function.h>
#include <Integration.h>

class sigma_tot : public Call_Function_Base {
protected:
	double mlep;
	double mlep2;
	double mp;
	double mp2;
//	double mpi2;
//	double hccm2;
//	double Gf2;
//	double cosc;
//	double pi;
	Integration_Class *dsdQ2;
	double precission;
	int iter;

public:
	//Constructor
	explicit sigma_tot(Integration_Class*, double precission=1e-42, int iter=1);
	//Destructor
	~sigma_tot() override;
	//Method
	double Call_Fnc(double) override; // Imput Enu in MeV
};

#endif //AXIALFF_SIGMA_TOT_H
