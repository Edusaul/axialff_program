//
// Created by eduardo on 11/06/18.
//

#include <Parameters_MeV.h>
#include "dsigmadQ2.h"

//Constructor
dsigmadQ2::dsigmadQ2(FFV *VFF,Call_Function_Base *AFF,bool Singh){
	this->pi= Param::pi;
    this->mp= Param::mp;
	this->mp2=this->mp*this->mp;
	this->mlep= Param::mmu;
	this->mlep2=this->mlep*this->mlep;
	this->mpi2= Param::mpi2;
	this->Gf2= Param::Gf2;
	this->hccm2= Param::hccm2;
	this->cosc= Param::cosc;
	this->VFF=VFF;
	this->AFF=AFF;
//	this->Enu=Enu;
	this->Singh=Singh;
	this->b_singh[0]=4.111e-1;
	this->b_singh[1]=2.3088e+1;
	this->b_singh[2]=-4.376346e+2;
	this->b_singh[3]=4.070158e+3;
	this->b_singh[4]=-1.799834e+4;
	this->b_singh[5]=3.019832e+4;
}

//Method
double dsigmadQ2::Call_Fnc(double Q2){
	double F1=this->VFF->Call_Fnc(Q2);
	F1=this->VFF->F1v;
	double F2=this->VFF->F2v;
	double FA=this->AFF->Call_Fnc(Q2);
	double FP=2.0*this->mp2*FA/(Q2+this->mpi2);
	double su=4.0*this->mp*this->Enu-Q2-this->mlep2; //su=s-u

// 	F1=0.0;
// 	F2=0.0;
// 	FA=0.0;
// 	FP=0.0;

	double eta=Q2/(4.0*this->mp2);

	double A=(this->mlep2+Q2)/this->mp2*((1.0+eta)*FA*FA-(1.0-eta)*F1*F1+eta*(1.0-eta)*F2*F2+4.0*eta*F1*F2-this->mlep2/(4.0*this->mp2)*((F1+F2)*(F1+F2)+(FA+2.0*FP)*(FA+2.0*FP)-(Q2/this->mp2+4.0)*FP*FP));

	double B=Q2/this->mp2*FA*(F1+F2);

	double C=1.0/4.0*(FA*FA+F1*F1+eta*F2*F2);

	double res=this->Gf2*this->mp2*this->cosc*this->cosc/(8.0*this->pi*this->Enu*this->Enu)*(A+su/this->mp2*B+su*su/(this->mp2*this->mp2)*C);

	double Q2GeV=Q2*1e-6;
	double singh_correction;
	if(Singh && Q2GeV < 0.2){
		singh_correction=this->b_singh[0]+this->b_singh[1]*Q2GeV+this->b_singh[2]*Q2GeV*Q2GeV+this->b_singh[3]*Q2GeV*Q2GeV*Q2GeV+this->b_singh[4]*Q2GeV*Q2GeV*Q2GeV*Q2GeV+this->b_singh[5]*Q2GeV*Q2GeV*Q2GeV*Q2GeV*Q2GeV;
	} else singh_correction=1.0;

// 	cout<<Q2<<" "<<FA<<" "<<res*this->hccm2<<endl;

	return res*this->hccm2*singh_correction; //  cm^2/MeV^2
}

int dsigmadQ2::Change_other_imputs(vector<double> Enu){
	this->Enu=Enu[0];
	return 0;
}

void dsigmadQ2::Change_bool_param(bool Singh){
	this->Singh=Singh;
}