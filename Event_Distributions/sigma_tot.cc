//
// Created by eduardo on 11/06/18.
//

#include <Parameters_MeV.h>
#include "sigma_tot.h"

//Destructor
sigma_tot::~sigma_tot() = default;

/////////////////////////////////////////////////////////////////////
//class sigma_tot
//Constructor
sigma_tot::sigma_tot(Integration_Class *dsdQ2, double precission, int iter){
	this->dsdQ2=dsdQ2;
	this->mp= Param::mp;
	this->mp2=this->mp*this->mp;
	this->mlep= Param::mmu;
	this->mlep2=this->mlep*this->mlep;
	this->precission=precission;
	this->iter=iter;
}

//Method
double sigma_tot::Call_Fnc(double Enu){
	if (Enu>this->mlep+this->mlep2/(2.0*this->mp)){
		double s=this->mp2+2.0*Enu*this->mp;
		double lkallen_in=(s-this->mp2)*(s-this->mp2);
		double lkallen_out=s*s+this->mlep2*this->mlep2+(this->mp2*this->mp2)-2.0*s*this->mlep2-2.0*s*this->mp2-2.0*this->mlep2*this->mp2;
		double t0=1.0/(4.0*s)*(this->mlep2*this->mlep2-(sqrt(lkallen_in)-sqrt(lkallen_out))*(sqrt(lkallen_in)-sqrt(lkallen_out)));
		double t1=1.0/(4.0*s)*(this->mlep2*this->mlep2-(sqrt(lkallen_in)+sqrt(lkallen_out))*(sqrt(lkallen_in)+sqrt(lkallen_out)));

		return this->dsdQ2->Integrate(-t0,-t1,this->precission,this->iter);
	} else{
		return 0.0;
	}
}