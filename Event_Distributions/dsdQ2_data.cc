//
// Created by eduardo on 11/06/18.
//

#include <Parameters_MeV.h>
#include <string>
#include <iostream>
#include <fstream>
#include "dsdQ2_data.h"

//Destructor
dsdQ2_data::~dsdQ2_data() = default;

//Constructor
dsdQ2_data::dsdQ2_data(string path, int nbins, double Nfit, int N_bin_divisions, double total_flux, double EnuMin, double EnuMax, double lep_momentum_cut, double data_factor, Integration_Class *ds_th){
	this->path=path;
	this->nbins=nbins;
	this->EnuMin=EnuMin;
	this->EnuMax=EnuMax;
	this->ds_th=ds_th;
// 	this->total_events=total_events;
	this->total_flux=total_flux;
	this->lep_momentum_cut=lep_momentum_cut;
	this->data_factor=data_factor;
	this->Nfit=Nfit;
	this->N_bin_divisions=N_bin_divisions;

	ifstream file(path.c_str());//opening imput document
	if(!file) cout<<"WARNING! flux file \""<<path<<"\" not found"<<endl;
	int i;
	double dataX,dataY,dataZ,dataSB;
	this->Q2.resize(static_cast<unsigned long>(this->nbins));
	this->dsdQ2.resize(static_cast<unsigned long>(this->nbins));
	this->var.resize(static_cast<unsigned long>(this->nbins));
	this->sizeBin.resize(static_cast<unsigned long>(this->nbins));
	this->th_dsdQ2.resize(static_cast<unsigned long>(this->nbins));
	for (i=0;i<nbins;i++){
		file>>dataX;
		file>>dataY;
		file>>dataZ;
		file>>dataSB;
		this->Q2[i]=dataX*1.0e6;
		this->dsdQ2[i]=dataY*this->data_factor;
		this->var[i]=dataZ*this->data_factor;
		this->sizeBin[i]=dataSB*1.0e6;

// 		cout<<this->Q2[i]<<" "<<this->dsdQ2[i]<<endl;
	}
	file.close();
}

//Method
void dsdQ2_data::diffCS(double precission, int iter){
// 	double dsdQ2center;
	vector<double> parameters(1);
	int i;
	double EnuMin_cut;
	double size_div;
	double bin_beggining;
	double dsdQ2_div;
	double dsdQ2_bin_avg;
	for (i=0;i<this->nbins;i++){
		size_div=this->sizeBin[i]/double(this->N_bin_divisions);
		bin_beggining=this->Q2[i]-this->sizeBin[i]/2.0;
		dsdQ2_bin_avg=0.0;
		for(int j=0;j<this->N_bin_divisions;j++){
			parameters[0]=bin_beggining+size_div/2.0;
			ds_th->Change_other_imputs(parameters);
			EnuMin_cut=this->lep_momentum_cut+parameters[0]/(2.0 * Param::mp);
			if(EnuMin_cut<this->EnuMin) EnuMin_cut=this->EnuMin;
			dsdQ2_div=ds_th->Integrate(EnuMin_cut,this->EnuMax,precission,iter)*size_div;
			dsdQ2_bin_avg += dsdQ2_div;
		}
		dsdQ2_bin_avg=dsdQ2_bin_avg/this->sizeBin[i];
		this->th_dsdQ2[i]=dsdQ2_bin_avg*this->Nfit/this->total_flux;

// 		parameters[0]=Q2[i];
// 		ds_th->Change_other_imputs(parameters);
// 		EnuMin_cut=this->lep_momentum_cut+Q2[i]/(2.0*Param::mp);
// 		cout<<EnuMin_cut<<"  "<<this->EnuMin<<"  ";
// 		if(EnuMin_cut<this->EnuMin) EnuMin_cut=this->EnuMin;
// 		cout<<EnuMin_cut<<endl;
// 		dsdQ2center=ds_th->Integrate(EnuMin_cut,this->EnuMax,precission,iter);
// 		cout<<"   dsdQ2 Integral = "<<dsdQ2center <<endl;
// 		this->th_dsdQ2[i]=dsdQ2center/this->total_flux;//*this->sizeBin[i];//*this->total_events;
	}

	//What I'm doing: For each BIN in Q2 I calculate the mean and multiply by the size of the bin the Nfit and divide by the flux integral.
	// to calculate the mean for each bin, I divide the bin in N_bin_divisions of Q2 and calculate the mean and multiply by the size of the minibin.
	// for the mean value of each minibin I do the integral in E_nu of dsdQ2*phi.

}