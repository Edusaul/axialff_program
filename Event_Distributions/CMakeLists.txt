cmake_minimum_required(VERSION 3.10)
project(Event_distributions_lib)

set(CMAKE_CXX_STANDARD 11)

add_library(Event_distributions_lib dsigmadQ2.cc dsigmadQ2.h
        sigma_tot.cc sigma_tot.h
        dNdQ2dEnu.cc dNdQ2dEnu.h
        dsigdQ2dEnu.cc dsigdQ2dEnu.h
        dNdQ2_data.cc dNdQ2_data.h
        dsdQ2_data.cc dsdQ2_data.h
        Call_Function_Base_w_bool.cpp Call_Function_Base_w_bool.h)

target_link_libraries(Event_distributions_lib libCall_Function_base.a libIntegration.a)