//
// Created by eduardo on 11/06/18.
//

#ifndef AXIALFF_DSIGDQ2DENU_H
#define AXIALFF_DSIGDQ2DENU_H

#include "dNdQ2dEnu.h"

//this is the class to do the averaged differential cross section for BEBC experiment
class dsigdQ2dEnu : public dNdQ2dEnu {
	public:
	//Constructor
	dsigdQ2dEnu(Call_Function_Base_w_bool*,Call_Function_Base*,Call_Function_Base*, int, double precission=1e-42, int iter=1);
	//Destructor
	~dsigdQ2dEnu() override;
	//Method
	double Call_Fnc(double) override; // Imput Enu in MeV

};

#endif //AXIALFF_DSIGDQ2DENU_H
