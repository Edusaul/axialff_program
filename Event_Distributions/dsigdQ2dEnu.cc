//
// Created by eduardo on 11/06/18.
//

#include <cmath>
#include "Call_Function_Base_w_bool.h"
#include "dsigdQ2dEnu.h"



//Destructor
dsigdQ2dEnu::~dsigdQ2dEnu() = default;

//Constructor
dsigdQ2dEnu::dsigdQ2dEnu(Call_Function_Base_w_bool *dsdQ2,Call_Function_Base *flux,Call_Function_Base *integrated_sigma, int Singh, double precission, int iter): dNdQ2dEnu(dsdQ2,flux,integrated_sigma, Singh, precission, iter) {}

//Method
double dsigdQ2dEnu::Call_Fnc(double Enu){
	vector<double> Enu_parameter(1);
	Enu_parameter[0]=Enu;
	// this gives a condition to the limits in Enu (for the lower limit)
	double cosTheta=(-0.5*sqrt(2.*Enu*this->mp + this->mp2)*(this->mlep2 - (1.*Enu*this->mp*(this->mlep2 + 2.*Enu*this->mp))/(2.*Enu*this->mp + this->mp2) + Q2))/
   (Enu*this->mp*sqrt(-1.*this->mlep2 + (0.25*(this->mlep2 + 2.*Enu*this->mp)*(this->mlep2 + 2.*Enu*this->mp))/(2.*Enu*this->mp + this->mp2)));

	if (Enu>this->thresold && cosTheta>-1.0 && cosTheta<1.0){

		this->dsdQ2->Change_other_imputs(Enu_parameter);
		if(this->Singh>0) this->dsdQ2->Change_bool_param(true);
		double aux_dsdQ2=this->dsdQ2->Call_Fnc(Q2);
		if(this->Singh<2) this->dsdQ2->Change_bool_param(false);
		double flx=this->flux->Call_Fnc(Enu);

		return flx*aux_dsdQ2;
	}
	else{
		return 0.0;
	}
}