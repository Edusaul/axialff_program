//
// Created by eduardo on 11/06/18.
//

#ifndef AXIALFF_DSDQ2_DATA_H
#define AXIALFF_DSDQ2_DATA_H

#include <Integration.h>

using namespace std;

class dsdQ2_data {
public:
	string path;
	int nbins;
	double Nfit;
	vector<double> Q2;
	vector<double> dsdQ2;
	vector<double> var;
	vector<double> sizeBin;
// 	double total_events; //total number of events data
	double total_flux; //total number of events flux
	double EnuMin; //for integratiion over the flux -> for normalization
	double EnuMax; //for integratiion over the flux -> for normalization
	Integration_Class *ds_th; //Theoretical distribution
	vector<double> th_dsdQ2;
	double lep_momentum_cut;
	double data_factor;
	int N_bin_divisions;

	//Constructor
	dsdQ2_data(string, int, double, int, double, double, double, double, double, Integration_Class*);
	//Destructor
	virtual ~dsdQ2_data();
	//Method
	virtual void diffCS(double precission=1e-3, int iter=10);
};


#endif //AXIALFF_DSDQ2_DATA_H
