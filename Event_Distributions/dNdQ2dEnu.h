//
// Created by eduardo on 11/06/18.
//

#ifndef AXIALFF_DNDQ2DENU_H
#define AXIALFF_DNDQ2DENU_H

#include <vector>

using namespace std;

class dNdQ2dEnu : public Call_Function_Base {
protected:
	double mlep;
	double mlep2;
	double mp;
	double mp2;
	double Q2;
	Call_Function_Base *integrated_sigma;
	Call_Function_Base_w_bool *dsdQ2;
	Call_Function_Base *flux;
	double thresold;
	double precission;
	int iter;
	int Singh;

public:
	//Constructor
	dNdQ2dEnu(Call_Function_Base_w_bool*,Call_Function_Base*,Call_Function_Base*, int, double precission=1e-42, int iter=1);
	//Destructor
	~dNdQ2dEnu() override;
	//Method
	double Call_Fnc(double) override; // Imput Enu in MeV
	int Change_other_imputs(vector<double>) override; //to change Q2
};

#endif //AXIALFF_DNDQ2DENU_H
