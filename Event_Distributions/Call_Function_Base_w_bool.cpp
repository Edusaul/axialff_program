//
// Created by edusaul on 11/06/18.
//

#include "Call_Function_Base_w_bool.h"

// Call_Function_Base with method to change bool parameter

//Constructor
Call_Function_Base_w_bool::Call_Function_Base_w_bool() = default;

//Destructor
Call_Function_Base_w_bool::~Call_Function_Base_w_bool() = default;

//Method
void Call_Function_Base_w_bool::Change_bool_param(bool){}
