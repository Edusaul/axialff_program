//
// Created by eduardo on 11/06/18.
//

#ifndef AXIALFF_DSIGMADQ2_H
#define AXIALFF_DSIGMADQ2_H

#include "Call_Function_Base_w_bool.h"
#include "FFV.h"

class dsigmadQ2 : public Call_Function_Base_w_bool {
protected:
	double mlep;
	double mlep2;
	double mp;
	double mp2;
	double mpi2;
	double hccm2;
	double Gf2;
	double cosc;
	double pi;
	double Enu;
	FFV *VFF;
	Call_Function_Base *AFF;
	bool Singh;
	double b_singh[6];//={4.111e-1,2.3088e+1,-4.376346e+2,4.070158e+3,-1.799834e+4,3.019832e+4};

public:
	//Constructor
	dsigmadQ2(FFV*,Call_Function_Base*,bool Singh=false);
	//Method
	double Call_Fnc(double) override; // Imput Q^2 in MeV^2
	int Change_other_imputs(vector<double>) override; //to change Enu
	void Change_bool_param(bool) override;
};

#endif //AXIALFF_DSIGMADQ2_H
