//
// Created by eduardo on 11/06/18.
//

#include <Parameters_MeV.h>
#include "Call_Function_Base_w_bool.h"
#include "dNdQ2dEnu.h"

//Destructor
dNdQ2dEnu::~dNdQ2dEnu() = default;

/////////////////////////////////////////////////////////////////////
//class dNdQ2dEnu
//Constructor
dNdQ2dEnu::dNdQ2dEnu(Call_Function_Base_w_bool *dsdQ2,Call_Function_Base *flux,Call_Function_Base *integrated_sigma, int Singh, double precission, int iter){
	this->dsdQ2=dsdQ2;
	this->flux=flux;
	this->integrated_sigma=integrated_sigma;
	this->mp= Param::mp;
	this->mp2=this->mp*this->mp;
	this->mlep= Param::mmu;
	this->mlep2=this->mlep*this->mlep;
	this->thresold=this->mlep+this->mlep2/(2.0*this->mp);
	this->Singh=Singh;
}

//Method
double dNdQ2dEnu::Call_Fnc(double Enu){
	vector<double> Enu_parameter(1);
	Enu_parameter[0]=Enu;
	// this gives a condition to the limits in Enu (for the lower limit)
	double cosTheta=(-0.5*sqrt(2.*Enu*this->mp + this->mp2)*(this->mlep2 - (1.*Enu*this->mp*(this->mlep2 + 2.*Enu*this->mp))/(2.*Enu*this->mp + this->mp2) + Q2))/
   (Enu*this->mp*sqrt(-1.*this->mlep2 + (0.25*(this->mlep2 + 2.*Enu*this->mp)*(this->mlep2 + 2.*Enu*this->mp))/(2.*Enu*this->mp + this->mp2)));

	if (Enu>this->thresold && cosTheta>-1.0 && cosTheta<1.0){

		this->dsdQ2->Change_other_imputs(Enu_parameter);
		if(this->Singh>0) this->dsdQ2->Change_bool_param(true);
		double aux_dsdQ2=this->dsdQ2->Call_Fnc(Q2);
		if(this->Singh<2) this->dsdQ2->Change_bool_param(false);
		double sigma_int= this->integrated_sigma->Call_Fnc(Enu);
		double flx=this->flux->Call_Fnc(Enu);

		return flx*aux_dsdQ2/sigma_int;
	}
	else{
		return 0.0;
	}
}

int dNdQ2dEnu::Change_other_imputs(vector<double> parameters){
	this->Q2=parameters[0];
	return 0;
}