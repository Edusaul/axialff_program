cmake_minimum_required(VERSION 3.10)
project(AxialFF)

set(CMAKE_CXX_STANDARD 11)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ROOTSYS}/etc/cmake)

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

#---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT REQUIRED COMPONENTS MathCore RIO Hist Tree Net Minuit)

#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
#include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include/Math)

include_directories(${ROOTSYS}/include/Math)

include_directories(inc)
link_directories(lib)

add_subdirectory(Form_Factors)
include_directories(Form_Factors)
add_subdirectory(Event_Distributions)
include_directories(Event_Distributions)
add_subdirectory(Flux)
include_directories(Flux)
add_subdirectory(Write_and_Plot)
include_directories(Write_and_Plot)
add_subdirectory(Minuit)
include_directories(Minuit)
add_subdirectory(Bootstrap)
include_directories(Bootstrap)


add_executable(AxialFF main.cpp)

target_link_libraries(AxialFF Form_Factors_lib)
target_link_libraries(AxialFF Event_distributions_lib)
target_link_libraries(AxialFF Flux_lib)
target_link_libraries(AxialFF Minuit_minimization_lib)
target_link_libraries(AxialFF Write_and_Plot_lib)
target_link_libraries(AxialFF Bootstrap_lib)



add_executable(AxialFF_Distributions Main_Distributions.cpp)

target_link_libraries(AxialFF_Distributions Form_Factors_lib)
target_link_libraries(AxialFF_Distributions Event_distributions_lib)
target_link_libraries(AxialFF_Distributions Flux_lib)
target_link_libraries(AxialFF_Distributions Write_and_Plot_lib)



add_executable(AxialFF_Minuit Main_Minuit.cpp)

target_link_libraries(AxialFF_Minuit Form_Factors_lib)
target_link_libraries(AxialFF_Minuit Event_distributions_lib)
target_link_libraries(AxialFF_Minuit Flux_lib)
target_link_libraries(AxialFF_Minuit Minuit_minimization_lib)


add_executable(AxialFF_error_function Main_error_function.cpp)

target_link_libraries(AxialFF_error_function Form_Factors_lib)
target_link_libraries(AxialFF_error_function Event_distributions_lib)
target_link_libraries(AxialFF_error_function Flux_lib)
target_link_libraries(AxialFF_error_function Minuit_minimization_lib)


