#include "Random_class.h"


//Constructor
random_number::random_number(char distribution){
	this->distribution=distribution;
//	cout<<"distribution "<<distribution<<endl;
	if((distribution!='u') && (distribution!='n')){
		cout<<"error: wrong distribution choice, setting normal distribution as default"<<endl;
		this->distribution='n';
	}
}
//Destructor
random_number::~random_number() = default;

//Method
int random_number::rand(int N_numbers, double a, double b){
	this->N_numbers=N_numbers;
	this->array_of_rnd_numbers.resize(static_cast<unsigned long>(this->N_numbers));
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	std::uniform_real_distribution<> u_dis(a, b); //rnd number in the interval [a,b)
	std::normal_distribution<> n_dis{a,b}; //rnd number, normal distribution with mean a and desv b
	for(int i=0;i<this->N_numbers;i++){
		
		if(this->distribution=='u'){
			array_of_rnd_numbers[i]=u_dis(gen);
		}else if(this->distribution=='n') array_of_rnd_numbers[i]=n_dis(gen);
	}
	
	return 0;
}
