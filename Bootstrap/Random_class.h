#ifndef _RANDOM_CLASS_H_
#define _RANDOM_CLASS_H_

#include <iostream>
#include <cmath>
#include <vector>
#include <random>
#include <map>
#include <iomanip>

using namespace std;

class random_number {
protected:
	char distribution; // 'u'->uniform, 'n'->normal(gaussian)
public:
	int N_numbers;
	std::vector<double> array_of_rnd_numbers;
	//Constructor
	explicit random_number(char);
	//Destructor
	virtual ~random_number();
	//Method
	int rand(int, double, double); 
};


#endif
