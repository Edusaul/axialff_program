#ifndef _BOOTSTRAP_H_
#define _BOOTSTRAP_H_

#include "Random_class.h"
#include "Plot.h"
#include "dNdQ2_data.h"
#include "dsdQ2_data.h"

class synthetic_dNdQ2_data : public dNdQ2_data {
protected:
	dNdQ2_data *original_data;

public:
	//Constructor
	explicit synthetic_dNdQ2_data(dNdQ2_data*);
	//Destructor
	~synthetic_dNdQ2_data() override;
	//Method
	double Generate_synct_data();
	void distEvents(double precission=1e-3, int iter=10) override;
};

class synthetic_dsdQ2_data : public dsdQ2_data {
protected:
	dsdQ2_data *original_data;

public:
	//Constructor
	explicit synthetic_dsdQ2_data(dsdQ2_data*);
	//Destructor
	~synthetic_dsdQ2_data() override;
	//Method
	double Generate_synct_data();
	void diffCS(double precission=1e-3, int iter=10) override;
};


#endif
