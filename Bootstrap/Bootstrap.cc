#include <Parameters_MeV.h>
#include "Bootstrap.h"


//class synthetic_dNdQ2_data

//Constructor
synthetic_dNdQ2_data::synthetic_dNdQ2_data(dNdQ2_data *original_data) : dNdQ2_data (original_data->path, original_data->nbins, original_data->Nfit, original_data->total_events, original_data->total_flux, original_data->EnuAv, original_data->EnuMin, original_data->EnuMax, original_data->Nth){
	this->original_data=original_data;
	this->Q2=this->original_data->Q2;
// 	this->dNdQ2=this->original_data->dNdQ2;
	this->var=this->original_data->var;
	this->sizeBin=this->original_data->sizeBin;
	this->th_dNdQ2=this->original_data->th_dNdQ2;
}
//Destructor
synthetic_dNdQ2_data::~synthetic_dNdQ2_data() = default;

//Method
double synthetic_dNdQ2_data::Generate_synct_data(){
	random_number rnd_nmb('n');
	int N_rnd=this->nbins;
	rnd_nmb.rand(N_rnd,0,1);
	for(int k=0;k<N_rnd;k++){
// 		cout<<rnd_nmb.array_of_rnd_numbers[k]<<" "<<k<<endl;
		this->dNdQ2[k]=this->original_data->dNdQ2[k] + rnd_nmb.array_of_rnd_numbers[k]*this->var[k];
// 		cout<<this->original_data->dNdQ2[k]<<" "<<this->dNdQ2[k]<<endl;
	}
	
	return 0;
}

void synthetic_dNdQ2_data::distEvents(double precission, int iter){
	double dNdQ2center;
	std::vector<double> parameters(1);
	int i;
	for (i=0;i<this->nbins;i++){
		parameters[0]=Q2[i];
		Nth->Change_other_imputs(parameters);
		dNdQ2center=Nth->Integrate(this->EnuMin,this->EnuMax,precission,iter)*this->Nfit/this->total_flux;
		this->th_dNdQ2[i]=dNdQ2center*this->sizeBin[i]*this->total_events;
	}
}

/////////////////////////////////////////////////////////////////////

//class synthetic_dsdQ2_data

//Constructor
synthetic_dsdQ2_data::synthetic_dsdQ2_data(dsdQ2_data *original_data) : dsdQ2_data (original_data->path, original_data->nbins, original_data->Nfit, original_data->N_bin_divisions, original_data->total_flux, original_data->EnuMin, original_data->EnuMax, original_data->lep_momentum_cut, original_data->data_factor, original_data->ds_th){
	this->original_data=original_data;
	this->Q2=this->original_data->Q2;
// 	this->dsdQ2=this->original_data->dsdQ2;
	this->var=this->original_data->var;
	this->sizeBin=this->original_data->sizeBin;
	this->th_dsdQ2=this->original_data->th_dsdQ2;
}
//Destructor
synthetic_dsdQ2_data::~synthetic_dsdQ2_data() = default;

//Method
double synthetic_dsdQ2_data::Generate_synct_data(){
	random_number rnd_nmb('n');
	int N_rnd=this->nbins;
	rnd_nmb.rand(N_rnd,0,1);
	for(int k=0;k<N_rnd;k++){
// 		cout<<rnd_nmb.array_of_rnd_numbers[k]<<" "<<k<<endl;
		this->dsdQ2[k]=this->original_data->dsdQ2[k] + rnd_nmb.array_of_rnd_numbers[k]*this->var[k];
// 		cout<<this->original_data->dsdQ2[k]<<" "<<this->dsdQ2[k]<<endl;
	}
	
	return 0;
}

void synthetic_dsdQ2_data::diffCS(double precission, int iter){
	std::vector<double> parameters(1);
	int i;
	double EnuMin_cut;
	double size_div;
	double bin_beggining;
	double dsdQ2_div;
	double dsdQ2_bin_avg;
	for (i=0;i<this->nbins;i++){
		size_div=this->sizeBin[i]/double(this->N_bin_divisions);
		bin_beggining=this->Q2[i]-this->sizeBin[i]/2.0;
		dsdQ2_bin_avg=0.0;
		for(int j=0;j<this->N_bin_divisions;j++){
			parameters[0]=bin_beggining+size_div/2.0;
			ds_th->Change_other_imputs(parameters);
			EnuMin_cut=this->lep_momentum_cut+parameters[0]/(2.0*Param::mp);
			if(EnuMin_cut<this->EnuMin) EnuMin_cut=this->EnuMin;
			dsdQ2_div=ds_th->Integrate(EnuMin_cut,this->EnuMax,precission,iter)*size_div;
			dsdQ2_bin_avg += dsdQ2_div;
		}
		dsdQ2_bin_avg=dsdQ2_bin_avg/this->sizeBin[i];
		this->th_dsdQ2[i]=dsdQ2_bin_avg*this->Nfit/this->total_flux;
	}
}
