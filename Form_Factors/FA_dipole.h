//
// Created by eduardo on 11/06/18.
//

#ifndef AXIALFF_FA_DIP_H
#define AXIALFF_FA_DIP_H

#include <Call_Function.h>
#include <vector>
using namespace std;

class FA_dipole : public Call_Function_Base {
protected:
	static double const ga;
	double MA; //MeV
public:
	//Method
	double Call_Fnc(double) override; // Imput Q^2 in MeV^2
	int Change_other_imputs(vector<double>) override;
};

#endif //AXIALFF_FA_DIP_H
