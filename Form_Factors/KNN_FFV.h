//
// Created by edusaul on 2/07/18.
//

#ifndef AXIALFF_KNN_FFV_H
#define AXIALFF_KNN_FFV_H

#include "FFV.h"

class KNN_FV : public FFV {
protected:

	static double const lambdap;//=1.793;
	static double const lambdan;//=-1.913;
	double wGEn[10];
	double wGEp[10];
	double wGMn[10];
	double wGMp[10];
// 	double mp2;
// 	int choice; // choice output for Call_Fnc method 1->F1, 2->F2

	//method
	double sigma(double);
	void fGEn(double i[2],double o[1]);
	void fGEp(double i[2],double o[1]);
	void fGMn(double i[2],double o[1]);
	void fGMp(double i[2],double o[1]);
// 	double FAAinner(double Q2);
	double FFV(double Q2);
public:
// 	double G[4];
// 	double F1v;
// 	double F2v;
	//Constructor
	explicit KNN_FV(int choice=1);
	//Destructor
	~KNN_FV() override;
	//Method
	double Call_Fnc(double) override; // Imput Q^2 in MeV^2
	int Change_other_imputs(vector<double>) override; //to choose the output of Call_Fcn
};

#include <iostream>
#include <Parameters_MeV.h>
#include "Call_Function.h"
#include "FFV.h"

#endif //AXIALFF_KNN_FFV_H
