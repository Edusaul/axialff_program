#ifndef _KNN_OUTPUT_H_
#define _KNN_OUTPUT_H_

using namespace std;

#include <cmath>
#include <cerrno>
#include <vector>
#include "Integration.h"
#include "Call_Function.h"


class KNN_FA : public Call_Function_Base {
protected:
	static const double ga;//=1.257;
	static const double MA2;//=1.0;
	double w[4];
// 	double sum_w;
// 	double alpha;
	//method
	double sigma(double);
	void f(double i[2],double o[1]);
	double FAA(double Q2 , double netOut);
public:
	//Constructor
	explicit KNN_FA();
	//Destructor
	~KNN_FA() override;
	//Method
	double Call_Fnc(double) override; // Imput Q^2 in MeV^2
};


class KNN_FA_NEW : public Call_Function_Base {
protected:
	static const double ga;//=1.257;
	static const double MA2;//=1.0;
	static const double MA2low;//=0.8*0.8;
	static const double MA2max;//=1.2*1.2;
	static const int FFpointer;// =0;
	static const int GAvaluefree;// =1;
	double w[13];

	//method
	double sigma(double);
	void f(double i[2],double o[1]);
	double FAAinner(double Q2);
	double FAA(double Q2 , double netOut);
public:
	//Constructor
	explicit KNN_FA_NEW();
	//Destructor
	~KNN_FA_NEW() override;
	//Method
	double Call_Fnc(double) override; // Imput Q^2 in MeV^2
};


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////



#endif
