//
// Created by eduardo on 11/06/18.
//


#include <Parameters_MeV.h>
#include "FA_dipole.h"

const double FA_dipole::ga= Param::ga;

//Method
double FA_dipole::Call_Fnc(double Q2){
	// Imput Q^2 in MeV^2
	return FA_dipole::ga/((1.0+Q2/(this->MA*this->MA))*(1.0+Q2/(this->MA*this->MA)));
}

int FA_dipole::Change_other_imputs(vector<double> Parameters){
	this->MA=Parameters[0]; //MeV
}
