#include "KNN_output.h"


const double KNN_FA::ga=1.257;
const double KNN_FA::MA2=1.0;

//Constructor
KNN_FA::KNN_FA(){
// 	double  w[4]={-0.71027535333023328334, 0.6544390929381327382, -1.1635136170176210157, 0.14164481571787052028};
	
	double w[4]={-7.2258433181853432004, 1.7594918809548223404, -6.7068296019064161939, -1.0163140478212495665};
// 	double w[4]={-0.71027535333023328334, 0.6544390929381327382, -1.1635136170176210157, 0.14164481571787052028};
	
	this->w[0]=w[0];
	this->w[1]=w[1];
	this->w[2]=w[2];
	this->w[3]=w[3];
	
// 	cout<<w[0]+w[1]+w[2]+w[3]<<endl;
// 	this->sum_w=w[0]+w[1]+w[2]+w[3];
// 	this->alpha=0.0185123639983220574;
}
//Destructor
KNN_FA::~KNN_FA() = default;

//Method
double KNN_FA::sigma(double x){
	return 1.0/(1.0+exp(-x));
}

void KNN_FA::f(double i[2],double o[1])
{
	double h[2];
	double x;

	i[1] = 1;
	x = this->w[0]*i[0] + this->w[1]*i[1]; 
	h[0]=sigma(x);
	x = 0; 
	h[1]=1;
	x = this->w[2]*h[0] + this->w[3]*h[1]; 
	o[0]=x;
}

double KNN_FA::FAA(double Q2 , double netOut)
{
	return (KNN_FA::ga - Q2*netOut)/pow(1. + Q2/ KNN_FA::MA2,2);
}

double KNN_FA::Call_Fnc(double Q2)
{
	double i[2], o[1]; //i[0]=Q2, o[0]=netOut

	i[0]=Q2*1.0e-6;
	f(i,o);	
	
	return FAA(i[0],o[0]);
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// Class KNN_FA_NEW

const double KNN_FA_NEW::ga=1.257;
const double KNN_FA_NEW::MA2=1.0;
const double KNN_FA_NEW::MA2low=0.8*0.8;
const double KNN_FA_NEW::MA2max=1.2*1.2;
const int KNN_FA_NEW::FFpointer =0;
const int KNN_FA_NEW::GAvaluefree =1;

//Constructor
KNN_FA_NEW::KNN_FA_NEW(){
	
// 	double w[13]={0.032804018243849014658, 0.093232588219681930553, 2.5978429075971072315, -0.57759226911018113348, 6.0608023508468429341, -2.8693959349810325143, 0.032801666539857152194, 0.09328362237768793308, -0.44884813009009094653, 6.976578947429362465, -3.3863393915122830258, -0.44893947486085300502, -0.85160841157260014356};
	
// 	double  w[13]={0.15552076942829623496, 0.026035732905014138949, 0.15551717797782196873, 0.026036491436255387866, 0.15551901948618168658, 0.026037012017901275951, 0.15551690109516766447, 0.026035243586502096469, 0.2902619361274750287, 0.29025968670725005305, 0.29026084751350123003, 0.29025970429716030363, 0.40270991786217963382};
	
	double  w[13]={-2.174, 0.1992, 2.141, -0.1948, -2.174, 0.1992, -5.481, 2.502, -2.502, 2.308, -2.502, 3.121, -0.1638};	
	
	for(int i=0;i<13;i++) {
		this->w[i]=w[i];
	}

}
//Destructor
KNN_FA_NEW::~KNN_FA_NEW() = default;

//Method
double KNN_FA_NEW::sigma(double x){
	return 1.0/(1.0+exp(-x));
}

void KNN_FA_NEW::f(double i[2],double o[1])
{
	double h[5];
	double x;
	
	i[1] = 1;
	x = this->w[0]*i[0] + this->w[1]*i[1]; h[0]=sigma(x);
	x = this->w[2]*i[0] + this->w[3]*i[1]; h[1]=sigma(x);
	x = this->w[4]*i[0] + this->w[5]*i[1]; h[2]=sigma(x);
	x = this->w[6]*i[0] + this->w[7]*i[1]; h[3]=sigma(x);
	x = 0; h[4]=1;
	x = this->w[8]*h[0] + this->w[9]*h[1] + this->w[10]*h[2] + this->w[11]*h[3] + this->w[12]*h[4]; o[0]=x;
	
}

double KNN_FA_NEW::FAAinner(double Q2)
{

	switch (KNN_FA_NEW::FFpointer)
	{
		case  0:
			return 1./pow(1. + Q2/ KNN_FA_NEW::MA2,2);

		case  1:
			return 1./pow(1. + Q2/ KNN_FA_NEW::MA2max,2);

		case -1:
			return 1./pow(1. + Q2/ KNN_FA_NEW::MA2low,2);


		default:
			return 1./pow(1. + Q2/ KNN_FA_NEW::MA2,2);
	}
}

double KNN_FA_NEW::FAA(double Q2 /* in GeV2 */, double netOut)
{
	switch(GAvaluefree)
	{
		case 0:
			return (KNN_FA_NEW::ga - Q2*netOut)*FAAinner(Q2);
			
		case 1:
			return KNN_FA_NEW::ga*netOut*FAAinner(Q2);
	
		default:
			return (KNN_FA_NEW::ga - Q2*netOut)*FAAinner(Q2);
	
	
	}
}

double KNN_FA_NEW::Call_Fnc(double Q2)
{
	double i[2], o[1]; //i[0]=Q2, o[0]=netOut

	i[0]=Q2*1.0e-6;
	f(i,o);	
	
	return FAA(i[0],o[0]);
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// Class KNN_FV


