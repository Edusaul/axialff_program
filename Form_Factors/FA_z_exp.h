//
// Created by edusaul on 12/06/18.
//

#ifndef AXIALFF_FA_Z_EXP_H
#define AXIALFF_FA_Z_EXP_H


#include <Call_Function.h>

class FA_z_exp : public Call_Function_Base {
protected:
    std::vector<double> a;
    double tcut;
    double t0op;

public:
    FA_z_exp();
    double Call_Fnc(double) override; // Imput Q^2 in MeV^2
	int Change_other_imputs(std::vector<double>) override;
	double calc_a0(std::vector<double>);
	double calc_a5(std::vector<double>);
	double calc_a6(std::vector<double>);
	double calc_a7(std::vector<double>);
	double calc_a8(std::vector<double>);
};


#endif //AXIALFF_FA_Z_EXP_H
