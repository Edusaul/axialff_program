//
// Created by eduardo on 11/06/18.
//

#include <Parameters_MeV.h>
#include "FA_2_monopoles.h"

const double FA_2_monopoles::ga= Param::ga;

//Method
double FA_2_monopoles::Call_Fnc(double Q2){
	// Imput Q^2 in MeV^2
	double MA1_2=this->MA*this->MA;
	double MA2_2=(this->MA+this->delta_MA)*(this->MA+this->delta_MA);
	return FA_2_monopoles::ga*(1.0+Q2/(MA1_2-MA2_2)*(MA2_2/(MA1_2+Q2)-MA1_2/(MA2_2+Q2)));
}

int FA_2_monopoles::Change_other_imputs(vector<double>Parameters){
	this->MA=Parameters[0]; //MeV
	this->delta_MA=Parameters[1]; //MeV
}