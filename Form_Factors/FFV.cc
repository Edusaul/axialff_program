//
// Created by eduardo on 11/06/18.
//

#include <Parameters_MeV.h>
#include <iostream>
#include "FFV.h"

const double FFV::lambdap=1.793;
const double FFV::lambdan=-1.913;
const double FFV::MD=843.0;
const double FFV::lambdanN=5.6;

//Destructor
FFV::~FFV() = default;

//Constructor
FFV::FFV(int choice){
	this->choice=choice;
	this->mp2= Param::mp2;
}

//Method
double FFV::Call_Fnc(double Q2){
	// Imput Q^2 in MeV^2
	double t=-Q2;
	double tau=Q2/(4.0*this->mp2);
	double mup=1.0+ FFV::lambdap;
	double mun= FFV::lambdan;

	double GEp= (1.0/(1.0-t/(FFV::MD* FFV::MD)))*(1.0/(1.0-t/(FFV::MD* FFV::MD)));
	double GMp=GEp*mup;
	double GMn=GEp*mun;
	double GEn=-GEp*mun*tau/(1.0+ FFV::lambdanN*tau);
	double F1p=(GEp+tau*GMp)/(1.0+tau);
	double F2p=(GMp-GEp)/(1.0+tau);
	double F1n=(GEn+tau*GMn)/(1+tau);
	double F2n=(GMn-GEn)/(1.0+tau);
	this->F1v=(F1p-F1n);
	this->F2v=(F2p-F2n);

	this->G[0]=GEn;
	this->G[1]=GEp;
	this->G[2]=GMn;
	this->G[3]=GMp;

	if(choice==1){
		return this->F1v;
	} else if(choice==2) {
		return this->F2v;
	} else {
		cout<<"Wrong choice in FFV, returning F1v"<<endl;
		return this->F1v;
	}
}

int FFV::Change_other_imputs(vector<double> choice){
	this->choice=int(choice[0]);
	return 0;
}