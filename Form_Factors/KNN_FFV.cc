//
// Created by edusaul on 2/07/18.
//

#include "KNN_output.h"
#include "KNN_FFV.h"

const double KNN_FV::lambdap=2.793;
const double KNN_FV::lambdan=-1.913;

//Destructor
KNN_FV::~KNN_FV() = default;

//Constructor
KNN_FV::KNN_FV(int choice){
	
	this->choice=choice;
	this->mp2= Param::mp2 * 1e-6; //GeV^2
	
	double  wGEn[7]={10.19704,2.36812,-1.144266,-4.274101,0.8149924,2.985524,-0.7864434};
	double  wGEp[10]={3.930227,0.1108384,-5.325479,-2.846154,-0.2071328,0.8742101,0.4283194,2.568322,2.577635,-1.185632};	
	double  wGMn[10]={3.19646,2.565681,6.441526,-2.004055,-0.2972361,3.606737,-3.135199,0.299523,1.261638,2.64747};
	double  wGMp[10]={-2.862682,-1.560675,2.321148,0.1283189,-0.2803566,2.794296,1.726774,0.861083,0.4184286,-0.1526676};
	
	for(int i=0;i<10;i++) {
		this->wGEp[i]=wGEp[i];
		this->wGMn[i]=wGMn[i];
		this->wGMp[i]=wGMp[i];
		if(i<7) this->wGEn[i]=wGEn[i];
	}

}

//Method
double KNN_FV::sigma(double x){
	return 1.0/(1.0+exp(-x));
}

void KNN_FV::fGEn(double i[2], double o[1])
{
	double h[3];
	double x;
	
	i[1] = 1;
	x = this->wGEn[0]*i[0] + this->wGEn[1]*i[1]; h[0]=sigma(x);
	x = this->wGEn[2]*i[0] + this->wGEn[3]*i[1]; h[1]=sigma(x);
	x = 0; h[2]=1;
	x = this->wGEn[4]*h[0] + this->wGEn[5]*h[1] + this->wGEn[6]*h[2]; o[0]=x;
}

void KNN_FV::fGEp(double i[2], double o[1])
{
	double h[4];
	double x;
	
	i[1] = 1;
	x = this->wGEp[0]*i[0] + this->wGEp[1]*i[1]; h[0]=sigma(x);
	x = this->wGEp[2]*i[0] + this->wGEp[3]*i[1]; h[1]=sigma(x);
	x = this->wGEp[4]*i[0] + this->wGEp[5]*i[1]; h[2]=sigma(x);
	x = 0; h[3]=1;
	x = this->wGEp[6]*h[0] + this->wGEp[7]*h[1] + this->wGEp[8]*h[2] + this->wGEp[9]*h[3]; o[0]=x;	
}

void KNN_FV::fGMn(double i[2], double o[1])
{
	double h[4];
	double x;
	
	i[1] = 1;
	x = this->wGMn[0]*i[0] + this->wGMn[1]*i[1]; h[0]=sigma(x);
	x = this->wGMn[2]*i[0] + this->wGMn[3]*i[1]; h[1]=sigma(x);
	x = this->wGMn[4]*i[0] + this->wGMn[5]*i[1]; h[2]=sigma(x);
	x = 0; h[3]=1;
	x = this->wGMn[6]*h[0] + this->wGMn[7]*h[1] + this->wGMn[8]*h[2] + this->wGMn[9]*h[3]; o[0]=x;	
}

void KNN_FV::fGMp(double i[2], double o[1])
{
	double h[4];
	double x;
	
	i[1] = 1;
	x = this->wGMp[0]*i[0] + this->wGMp[1]*i[1]; h[0]=sigma(x);
	x = this->wGMp[2]*i[0] + this->wGMp[3]*i[1]; h[1]=sigma(x);
	x = this->wGMp[4]*i[0] + this->wGMp[5]*i[1]; h[2]=sigma(x);
	x = 0; h[3]=1;
	x = this->wGMp[6]*h[0] + this->wGMp[7]*h[1] + this->wGMp[8]*h[2] + this->wGMp[9]*h[3]; o[0]=x;	
}

double KNN_FV::FFV(double Q2 /* in GeV2 */)
{
	double tau=Q2/(4.0*this->mp2);
	double GEn=this->G[0];
	double GEp=this->G[1];
	double GMn=this->G[2];
	double GMp=this->G[3];
	
	double F1p=(GEp+tau*GMp)/(1.0+tau);
	double F2p=(GMp-GEp)/(1.0+tau);
	double F1n=(GEn+tau*GMn)/(1+tau);
	double F2n=(GMn-GEn)/(1.0+tau);
	this->F1v=(F1p-F1n);
	this->F2v=(F2p-F2n);	
	
	Q2=Q2*1e6;
	double t=-Q2;
	tau=Q2/(4.0*this->mp2*1e6);
	double mup= KNN_FV::lambdap;
	double mun= KNN_FV::lambdan;
// 	cout<<Q2<<endl;
	
// 	GEp= (1.0/(1.0-t/(this->MD*this->MD)))*(1.0/(1.0-t/(this->MD*this->MD)));
// 	 GMp=GEp*mup;
// 	 GMn=GEp*mun;
// 	 GEn=-GEp*mun*tau/(1.0+this->lambdanN*tau);
// 	 F1p=(GEp+tau*GMp)/(1.0+tau);
// 	 F2p=(GMp-GEp)/(1.0+tau);
// 	 F1n=(GEn+tau*GMn)/(1.0+tau);
// 	 F2n=(GMn-GEn)/(1.0+tau);
// 	this->F1v=(F1p-F1n);
// 	this->F2v=(F2p-F2n);	
// 	this->G[0]=GEn;
// 	this->G[1]=GEp;
// 	this->G[2]=GMn;
// 	this->G[3]=GMp;
	
	
// 	FFV ffv_dip(1);
// 	ffv_dip->Call_Fnc(Q2*1e6);
// 	this->F1v=ffv_dip->F1v;
	
	if(choice==1){
		return this->F1v;
	} else if(choice==2) {
		return this->F2v;
	} else {
		cout<<"Wrong choice in FFV, returning F1v"<<endl;
		return this->F1v;
	}
}

double KNN_FV::Call_Fnc(double Q2)
{
	double i[2], o[1]; //i[0]=Q2, o[0]=netOut
	
	i[0]=Q2*1.0e-6;
	
	double MV=0.71;
	double MV2=MV;//*MV;
	double dipole=1.0/((1.0+i[0]/MV2)*(1.0+i[0]/MV2));
	
	fGEn(i,o);	
	this->G[0]=o[0];
	fGEp(i,o);	
	this->G[1]=o[0]*dipole;
	fGMn(i,o);	
	this->G[2]=o[0]*dipole*lambdan;
	fGMp(i,o);	
	this->G[3]=o[0]*dipole*lambdap;
	
	return FFV(i[0]);
}

int KNN_FV::Change_other_imputs(vector<double> choice){
	this->choice=int(choice[0]);
	return 0;
}