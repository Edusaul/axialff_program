//
// Created by eduardo on 11/06/18.
//

#ifndef AXIALFF_FFV_H
#define AXIALFF_FFV_H

#include "Call_Function.h"
#include <vector>

using namespace std;

class FFV : public Call_Function_Base {
protected:
	static double const lambdap;//=1.793;
	static double const lambdan;//=-1.913;
	static double const MD;//=843.0; //MeV
	static double const lambdanN;//=5.6;
	double mp2;
	int choice; // choice output for Call_Fnc method 1->F1, 2->F2
public:
	double F1v;
	double F2v;
	double G[4];
	//Constructor
	explicit FFV(int choice=1);
	//Destructor
	~FFV() override;
	//Method
	double Call_Fnc(double) override; // Imput Q^2 in MeV^2
	int Change_other_imputs(vector<double>) override; //to choose the output of Call_Fcn
};


#endif //AXIALFF_FFV_H
