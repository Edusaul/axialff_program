//
// Created by eduardo on 11/06/18.
//

#include <Parameters_MeV.h>
#include "FA_1_monopole.h"

const double FA_1_monopole::ga= Param::ga;

//Method
double FA_1_monopole::Call_Fnc(double Q2){
	// Imput Q^2 in MeV^2
	return FA_1_monopole::ga/(1.0+Q2/(this->MA*this->MA));
}

int FA_1_monopole::Change_other_imputs(vector<double> Parameters){
	this->MA=Parameters[0]; //MeV
}