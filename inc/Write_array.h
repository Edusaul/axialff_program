//
// Created by edusaul on 17/06/18.
//

#ifndef LIBRARIES_WRITE_ARRAY_H
#define LIBRARIES_WRITE_ARRAY_H


class Write_array {
protected:
    std::vector<std::vector<double>> data;
    std::string file_path;
    std::string plot_path;

public:

    explicit Write_array(std::vector<std::vector<double>> &data, const std::string &file_path,
                const std::string &plot_path);

    virtual void write(std::vector<int> columns = {0});
    virtual void Math_Plot(std::vector<int>, std::string AxesLabel="{}", std::string PlotRange="All");
    virtual void gnuplot_Plot(std::vector<int>);

};

#endif //LIBRARIES_WRITE_ARRAY_H
