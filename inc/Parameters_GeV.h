#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_
#include <cmath>

namespace Param
{


    const double pi= 4.0*atan(1.0);
    const double alpha= 1.0/137.0;
    const double hc=0.19733; // (GeV x fm)
    const double hccm=197.33e-16; // (GeV x cm)
    const double hbar=6.58212e-25; // (GeV x s)
    const double Gf=1.166e-5; // GeV^-2
    const double cosc=0.975;       // Cabbibo angle
    const double cosw2=0.23122;    // Weak angle squared
    const double sinw2=0.23126;
    const double cvacuum=3.0e8; //(m/s) light speed in vacuum
    const double cvacuumcm=3.0e10; //(cm/s) light speed in vacuum
    const double NA=6.022e23; //Avogadro's number
    const double Vus=0.2252; //matrix element of CKM matrix
    const double ga=1.2723; //from the PDG value for gA/gV (measured in beta decay: http://pdglive.lbl.gov/DataBlock.action?node=S017AV&init=0) using gV=1 as expected.
    const double F_chiPT=0.463;
    const double D_chiPT=0.804;

    //lepton mases
    const double me= 0.51099892e-3; //GeV
    const double mmu= 0.1056583715; //GeV

    //messon mass
    const double mpi=0.13957; //GeV
    const double mk=0.493677; //GeV

    //form factor
    const double fk=0.113; //GeV kaon decay constant
    const double fpi=0.0924; //GeV pion decay constant

    //nucleon
    const double mp= 0.93827203; //GeV
    const double mn= 0.93827203; //GeV
    const double mup = 1.7928; //proton anomalous magnetic moment
    const double mun = -1.913; //neutron anomalous magnetic moment

    //Carbon nucleus
    const double AC= 12.0;
    const double ZC= 6.0;
    const double MC= 12.0*mp;

    //Powers

    const double hccm2= hccm*hccm;
    const double Gf2=Gf*Gf;
    const double mpi2=mpi*mpi;
    const double mp2=mp*mp;
    const double mk2=mk*mk;

}

#endif
