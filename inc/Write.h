//
// Created by edusaul on 21/05/18.
//

#ifndef LIBRARIES_WRITE_H
#define LIBRARIES_WRITE_H


class Write {
protected:
    std::vector<double> data_x;
    std::vector<double> data_y;
    std::string file_path;
    std::string plot_path;

public:
    Write(const std::vector<double> &data_x, const std::vector<double> &data_y, std::string file_path = "data.dat",
          std::string plot_path = "Plot.pdf");

    virtual void write();
    virtual void Math_Plot(std::string AxesLabel="{}", std::string PlotRange="All");
    virtual void gnuplot_Plot();
};




#endif //LIBRARIES_WRITE_H
