#ifndef INTEGRATION_H
#define INTEGRATION_H

#include <vector>

using namespace std;

class Call_Function_Base;

class Integration_Class
{
protected:
	static double defdokl;
	static double const a;//=0.53846931010568309105; //sqrt(245-14*sqrt(70))/21
	static double const b;//=0.90617984593866399282; //sqrt(245+14*sqrt(70))/21
	static double const f0;//=128.0/225;
	static double fb; // 0.23692689
	static double fa; // 0.47862867
	int lcalg5;
	Call_Function_Base *Function_to_integrate;
public:
	int iteration;
	//Constructor
	explicit Integration_Class(Call_Function_Base*);
	//Destructor
	virtual ~Integration_Class();
	//Method
    virtual double Integrate(double, double, double dokl=1e-12,int ile=1);
	virtual int Change_other_imputs(std::vector<double>);
};

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// Gauss with 20 points

class Integration_Class_Gauss20 : public Integration_Class
{
protected:
	static const double w20[10];
	static const double g20[10];
	Call_Function_Base *Function_to_integrate;
public:
	//Constructor
    explicit Integration_Class_Gauss20(Call_Function_Base*);
	//Destructor
	~Integration_Class_Gauss20() override;
	//Method
	double Integrate(double, double, double useless=1, int n=1) override;
};


#endif // INTEGRATION_H
